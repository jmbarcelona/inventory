$(".btn_register").on('click', function(e){
	var url = $(this).data('url');
	var group_id = $(this).data('group_id');
	var seminar_id = $(this).data('seminar_id');
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
	    	type:"POST",
	        url:url,
	        data:{group_id:group_id,seminar_id:seminar_id},
	        cache:false,
	        beforeSend:function(){
	           $("#btn_register").prop('disabled', true);
	        },
	    	success:function(response){
	        if(response.status == true){
		      	window.location = response.redirect;
	        }else{

	        	if(response.type == 'capacity_full'){
	        		window.location = response.redirect;
	        	}else{
					$("#btn_register").prop('disabled', false);
					location.reload();
	        	}
	        }
	    },
	    error:function(error){
	    	location.reload();
	        $("#btn_register").prop('disabled', false);
	    }
	});
});

$(".btn_cancel").on('click', function(e){

	var url = $(this).data('url');
	var seminar_registration_id = $(this).data('id');
	var seminar_id = $(this).data('seminar_id');
    e.stopPropagation();
    e.preventDefault(e);
    $.ajax({
	    	type:"POST",
	        url:url,
	        data:{seminar_registration_id:seminar_registration_id,seminar_id:seminar_id},
	        cache:false,
	        beforeSend:function(){
	           $(this).prop('disabled', true);
	        },
	    	success:function(response){
	        if(response.status == true){
		       	 window.location = response.redirect;
	        }else{
		        $(this).prop('disabled', false);
	        }
	    },
	    error:function(error){
	    	location.reload();
	        $(this).prop('disabled', false);
	    }
	});
});