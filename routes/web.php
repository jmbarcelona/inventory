<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\SupplierController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\UserAccountController;
use App\Http\Controllers\StockController;
use App\Http\Controllers\LogController;
use App\Http\Controllers\SupportController;

 
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('Auth.index');
})->name('login');




//Authentications/Login
Route::group(['prefix'=> 'auth', 'as' => 'auth.'], function(){
	// For Login
	Route::controller(AuthController::class)->group(function () {
 		 Route::post('request', 'authenticate')->name('authenticate');
	});
});


Route::group(['prefix' => 'user_accounts', 'as' => 'user_accounts.'], function(){
	Route::controller(UserAccountController::class)->group(function () {
		Route::get('', 'index')->name('index');
		Route::get('list', 'list')->name('list');
		Route::post('save/{id?}', 'save')->name('save');
		Route::get('find/{id?}', 'find')->name('find');
		Route::get('toggle/{id?}/{status?}', 'toggle')->name('toggle');
	});
});


Route::group(['middleware' => ['auth']], function(){
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    // Dashboard
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
 


	// Brands
Route::group(['prefix' => 'brands', 'as' => 'brands.'], function(){
	Route::controller(BrandController::class)->group(function () {
		Route::get('', 'index')->name('index');
		Route::get('list', 'list')->name('list');
		Route::post('save/{id?}', 'save')->name('save');
		Route::get('find/{id?}', 'find')->name('find');
		Route::delete('delete/{id?}', 'delete')->name('delete');
	});
});
	// Category
	Route::group(['prefix' => 'categories', 'as' => 'categories.'], function(){
		Route::controller(CategoryController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});

	


	//product
	Route::group(['prefix' => 'products', 'as' => 'products.'], function(){
		Route::controller(ProductController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::get('add', 'add')->name('add');
			Route::get('edit/{id?}', 'edit')->name('edit');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});

	// Material
	Route::group(['prefix' => 'materials', 'as' => 'materials.'], function(){
		Route::controller(MaterialController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});

	//supplier
	Route::group(['prefix' => 'suppliers', 'as' => 'suppliers.'], function(){
		Route::controller(SupplierController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});


	//customer
	Route::group(['prefix' => 'customers', 'as' => 'customers.'], function(){
		Route::controller(CustomerController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
		});
	});

	//orders
	Route::group(['prefix' => 'orders', 'as' => 'orders.'], function(){
		Route::controller(OrderController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::post('items', 'getItemFromCode')->name('get.items');
			Route::get('list', 'list')->name('list');
			Route::get('add', 'add')->name('add');
			Route::get('edit/{id?}', 'edit')->name('edit');
			Route::post('save/{id?}', 'save')->name('save');
			Route::get('find/{id?}', 'find')->name('find');
			Route::delete('delete/{id?}', 'delete')->name('delete');
			Route::post('payment', 'payment')->name('payment');
		});
	});

	// Stocks

	Route::group(['prefix' => 'stocks', 'as' => 'stocks.'], function(){
		Route::controller(StockController::class)->group(function () {
			Route::get('{type?}', 'index')->name('index');
			Route::get('list/{type?}', 'list')->name('list');
			Route::post('save/{stock_id?}', 'save')->name('save');
			Route::get('find/{stock_id?}', 'find')->name('find');
			Route::get('find-stocks/{id?}/{type?}', 'findItems')->name('find.items');
			Route::get('find-stocks-code/{code?}/{type?}', 'findItemsCode')->name('find.items.code');
			Route::delete('delete/{stock_id?}', 'delete')->name('delete');
		});
	});

	//activity logs
	Route::group(['prefix' => 'logs', 'as' => 'logs.'], function(){
		Route::controller(LogController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
		});
	});

	//activity logs
	Route::group(['prefix' => 'support', 'as' => 'support.'], function(){
		Route::controller(SupportController::class)->group(function () {
			Route::get('', 'index')->name('index');
			Route::get('list', 'list')->name('list');
		});
	});


});


/* Put it at the top */
