<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Log;
use Validator;
use Auth;

class LogController extends Controller
{
	public function index(){
		return view('Log.index');
	}

	public function list(){
		$logs = Log::with(['user'])->get();
		return response()->json(['status' => true, 'data' => $logs ]);
	}
}