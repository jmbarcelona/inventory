<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Supplier;
use Validator;
use Auth;

class MaterialController extends Controller
{
	public function index(){
		$brand = Brand::whereNull('deleted_at')->get();
		$category = Category::whereNull('deleted_at')->get();
		$supplier = Supplier::whereNull('deleted_at')->get();

		return view('Material.index', compact('brand','category','supplier'));
	}

	public function list(){

		$materials = Material::with(['brands', 'suppliers', 'categories'])->whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $materials ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;								
		$validator = Validator::make($request->all(), [
			'item_code' => 'required|unique:materials,item_code,'.$id,
			'material_name' => 'required',
			'alert_qty' => 'required|numeric|min:1',
			'category' => 'required',
			'supplier' => 'required',
			'brand' => 'required',
			'price_per_kilo' => 'required|numeric|gt:0',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$materials = Material::where('id', $id)->update([
					'item_code' => $request->get('item_code'),
					'material_name' => $request->get('material_name'),
					'category_id' => $request->get('category'),
					'supplier_id' => $request->get('supplier'),
					'brand_id' => $request->get('brand'),
					'price_per_kilo' => $request->get('price_per_kilo'),
				]);
				if($materials){
					makeLog($user_id, 'Updated Material', 'Updated Material named '.$request->material_name.' with the ID:'.$id);
					return response()->json(['status' => true, 'message' => 'materials saved successfully!']);
				}
			}else{
				$materials = Material::create([
					'item_code' => $request->get('item_code'),
					'material_name' => $request->get('material_name'),
					'category_id' => $request->get('category'),
					'supplier_id' => $request->get('supplier'),
					'brand_id' => $request->get('brand'),
					'price_per_kilo' => $request->get('price_per_kilo'),
				]);
				if($materials){
					makeLog($user_id, 'Added Material', 'Added Material named '.$request->material_name.' with the ID:'.$materials->id);
					return response()->json(['status' => true, 'message' => 'materials updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$materials = Material::findOrFail($id);
		return response()->json(['status' => true, 'data' => $materials ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;								
		
		$materials = Material::findOrFail($id);
		$materials->deleted_at = now();
		if($materials->save()){
			makeLog($user_id, 'Deleted Material', 'Deleted Material named '.$request->material_name.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}