<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Validator;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class AuthController extends Controller
{


	public function index(){
			return view('Dashboard.index');
	}

	public function authenticate(Request $request){
		 $validator = Validator::make($request->all(), [
            'email_address' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => false, 'error' => $validator->errors()]);
        }else{
        	$credentials = $request->validate([
	            'email_address' => ['required', 'email'],
	            'password' => ['required'],
	        ]);

	        if (Auth::attempt($credentials)) {
	            $request->session()->regenerate();
	            $user = Auth::user();
	            
	            if ($user->user_type == '1') {
	            	$redirect = route('dashboard');
	            }else{
	            	$redirect = route('dashboard');
	            }

	            return response()->json(['status' => true, 'redirect' => $redirect ]);
	        }else{
	        	$validator->errors()->add('password','Invalid Account!');
	            return response()->json(['status' => false, 'error' => $validator->errors()]);
	        }
        }
	}

	public function logout()
    {
		$user = Auth::user();
		Session::flush();
    	Auth::logout();
		return redirect(route('login'));
		// if(save_auth_logs($user->id,'logout')) {
			
		// }
    }
}