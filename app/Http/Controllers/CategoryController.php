<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Category;
use Validator;
use Auth;
class CategoryController extends Controller
{
	public function index(){
		return view('Category.index');
	}

	public function list(){
		$categories = Category::get();
		return response()->json(['status' => true, 'data' => $categories ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;				
		$validator = Validator::make($request->all(), [
			'category' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$categories = Category::where('id', $id)->update([
					'category' => $request->get('category'),
				]);
				if($categories){
					makeLog($user_id, 'Updated Category', 'Updated Category named '.$request->category.' with the ID:'.$id);
					return response()->json(['status' => true, 'message' => 'categories saved successfully!']);
				}
			}else{
				$categories = Category::create([
					'category' => $request->get('category'),
				]);
				if($categories){
					makeLog($user_id, 'Added Category', 'Added Category named '.$categories->category.' with the ID:'.$categories->id);
					return response()->json(['status' => true, 'message' => 'categories updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$categories = Category::findOrFail($id);
		return response()->json(['status' => true, 'data' => $categories ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;		
		$categories = Category::findOrFail($id);
		if($categories->delete()){
			makeLog($user_id, 'Deleted Category', 'Deleted Category named '.$categories->category.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}