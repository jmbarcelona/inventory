<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Brand;
use Validator;
use Auth;

class BrandController extends Controller
{
	public function index(){
		return view('Brand.index');
	}

	public function list(){
		$brands = Brand::get();
		return response()->json(['status' => true, 'data' => $brands ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;
		$validator = Validator::make($request->all(), [
			'brand' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$brands = Brand::where('id', $id)->update([
					'brand' => $request->get('brand'),
				]);
				if($brands){
					makeLog($user_id, 'Update Brand Name', 'Updated Brand name to '.$request->get('brand').' with the ID:'.$id);
					return response()->json(['status' => true, 'message' => 'brands saved successfully!']);
				}
			}else{
				$brands = Brand::create([
					'brand' => $request->get('brand'),
				]);
				if($brands){
					makeLog($user_id, 'Added Brand', 'Added Brand to '.$request->get('brand').' with the ID:'.$brands->id);
					return response()->json(['status' => true, 'message' => 'brands updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$brands = Brand::findOrFail($id);
		return response()->json(['status' => true, 'data' => $brands ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;
		$brands = Brand::findOrFail($id);
		if($brands->delete()){
			makeLog($user_id, 'Delete Brand', 'Deleted Brand named '.$brands->brand.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}