<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\Stock;
use Validator;
use Auth;

class OrderController extends Controller
{
	public function index(){
		return view('Order.index');
	}

	public function list(){
		$orders = Order::with(['customer'])->whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $orders ]);
	}

	public function getItemFromCode(Request $request){
		$validator = Validator::make($request->all(), [
			'item_code' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			$product = Product::where('product_code', $request->get('item_code'));

			if ($product->count() > 0) {
				$product_details = $product->first();

				if ($product_details->qty > 0) {
					return response()->json(['status' => true, 'data' => $product_details]);
				}else{
					$validator->errors()->add('item_code', 'Insufficient stock!');
					return response()->json(['status' => false, 'error' => $validator->errors() ]);
				}

			}else{
				$validator->errors()->add('item_code', 'This Item is not registered to the system!');
				return response()->json(['status' => false, 'error' => $validator->errors() ]);
			}
		}
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;										
		$validator = Validator::make($request->all(), [
			'customer' => 'required',
			'delivery_date' => 'required',
			'total_bill' => 'required'
		], [
			'total_bill.required' => 'Please add items to proceed!'
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			// return response()->json(['status' => false, 'data' => $request->all()]);
			if(!empty($id)){
				$orders = Order::where('id', $id)->update([
					'customer_id' => $request->get('customer'),
					'delivery_date' => $request->get('delivery_date'),
					'total_amount' => $request->get('total_bill'),
				]);
				if($orders){
					makeLog($user_id, 'Updated Order', 'Updated Order with the ID:'.$id);
					$this->saveOrderDetails($request->get('items'), $id);
					return response()->json(['status' => true, 'message' => 'orders saved successfully!']);
				}
			}else{
				$orders = Order::create([
					'customer_id' => $request->get('customer'),
					'delivery_date' => $request->get('delivery_date'),
					'order_tracking_no' => getOrderSlug(),
					'status' => 0,
					'total_amount' => $request->get('total_bill'),

				]);
				if($orders){
					makeLog($user_id, 'Added Order', 'Added Order with the ID:'.$orders->id);
					$this->saveOrderDetails($request->get('items'), $orders->id);
					return response()->json(['status' => true, 'message' => 'orders updated successfully!']);
				}
			}
		}
	}

	public function payment(Request $request){
		$user_id = Auth::user()->id;										
		$validator = Validator::make($request->all(), [
				'bill' => 'required',
				'order_id' => 'required',
				'amount_tendered' => 'required|numeric|min:'.$request->bill,
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			$check_stock_deduction = $this->deductProduct($request->get('order_id'));

			if ($check_stock_deduction['error'] > 0) {
					return response()->json(['status' => false, 'message' => 'Cannot process your payment please check stocks.']);
			}else{
				$order = Order::where('id', $request->get('order_id'))->update([
					'total_amount' => $request->get('bill'),
					'amount_tendered' => $request->get('amount_tendered'),
					'amount_changed' => $request->get('change'),
					'status' => 1,
				]);

				if ($order) {
					return response()->json(['status' => true, 'message' => 'Payment processed successfully!']);
				}
			}
		}
	}

	public function deductProduct($id){
		$error = 0;
		$success = 0;
		$user_id = Auth::user()->id;

		$details = OrderDetail::where('order_id', $id)->get();
		foreach ($details as $detail) {
			$product_id =  $detail->product_id;
			$qty =  $detail->qty;
			$updated_qty = $this->updated_product($product_id, $qty);

			Product::where('id', $product_id)->update(['qty' => $updated_qty['updated_qty']]);

			$stocks = Stock::create([
						'product_id' => $product_id,
						'qty' => $qty,
						'status' => 'stock_out',
						'note' => 'Deducted for order ID:'. $product_id,
						'user_id' => $user_id,
						'qty_at_that_time' => $updated_qty['current_qty'],
						'updated_qty' => $updated_qty['updated_qty'],
					]);

			return ['error' => $error, 'success' => $success];
		}
	}

	public function updated_product($product_id, $qty){
			$product = Product::find($product_id);
			return ['updated_qty' => ($product->qty - $qty), 'current_qty' => $product->qty];
	}

	public function saveOrderDetails($details, $id){
		$clear = OrderDetail::where('order_id', $id)->delete();
		$counter = 0;
		foreach ($details as $key => $data) {
			OrderDetail::create(['order_id' => $id, 'product_id' => $data['id'], 'qty' => $data['qty'], 'price' => $data['price']]);
			$counter++;
		}
		
		return $counter;
	}


	public function edit($id){
		$title = 'Edit Order';
		$orders = Order::with(['items', 'items.products'])->findOrFail($id);
		return view('Order.add', compact('orders', 'title'));
	}

	public function add(){
		$title = 'Add Order';
		return view('Order.add', compact('title'));
	}

	public function find($id){
		$orders = Order::findOrFail($id);
		return response()->json(['status' => true, 'data' => $orders ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;												
		$orders = Order::findOrFail($id);
		$orders->deleted_at = now();
		if($orders->save()){
			makeLog($user_id, 'Deleted Order', 'Deleted Order with the ID:'.$orders->id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}