<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Supplier;
use Validator;
use Auth;

class SupplierController extends Controller
{
	public function index(){
		return view('Supplier.index');
	}

	public function list(){
		$suppliers = Supplier::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $suppliers ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;								
		$validator = Validator::make($request->all(), [
			'supplier' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$suppliers = Supplier::where('id', $id)->update([
					'supplier' => $request->get('supplier'),
				]);
				if($suppliers){
					makeLog($user_id, 'Updated Supplier', 'Updated Supplier named '.$request->supplier.' with the ID:'.$id);
					return response()->json(['status' => true, 'message' => 'suppliers saved successfully!']);
				}
			}else{
				$suppliers = Supplier::create([
					'supplier' => $request->get('supplier'),
				]);
				if($suppliers){
					makeLog($user_id, 'Added Supplier', 'Added Supplier named '.$request->supplier.' with the ID:'.$suppliers->id);
					return response()->json(['status' => true, 'message' => 'suppliers updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$suppliers = Supplier::findOrFail($id);
		return response()->json(['status' => true, 'data' => $suppliers ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;										
		$suppliers = Supplier::findOrFail($id);
		$suppliers->deleted_at = now();
		if($suppliers->save()){
			makeLog($user_id, 'Deleted Supplier', 'Deleted Supplier named '.$suppliers->supplier.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}