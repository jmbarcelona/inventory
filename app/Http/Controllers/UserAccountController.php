<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;
use Validator;
use Auth;

class UserAccountController extends Controller
{
	public function index(){
		return view('User.index');
	}

	public function list(){
		$user_accounts = User::where('user_type', '!=', 1)->get();
		return response()->json(['status' => true, 'data' => $user_accounts ]);
	}

	public function save(Request $request, $id = ""){
		$valid_pass = (!empty($id))? '' : 'required|min:8';

		$validator = Validator::make($request->all(), [
			'name' => 'required',
			'gender' => 'required',
			'email_address' => 'required|email',
			'password' => $valid_pass,
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$user_accounts = User::where('id', $id)->update([
					'name' => $request->get('name'),
					'gender' => $request->get('gender'),
					'email_address' => $request->get('email_address'),
				]);
				if($user_accounts){
					return response()->json(['status' => true, 'message' => 'user accounts saved successfully!']);
				}
			}else{
				$user_accounts = User::create([
					'name' => $request->get('name'),
					'gender' => $request->get('gender'),
					'email_address' => $request->get('email_address'),
					'password' => $request->get('password'),
					'user_type' => 2,
				]);
				if($user_accounts){
					return response()->json(['status' => true, 'message' => 'user accounts updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$user_accounts = User::findOrFail($id);
		return response()->json(['status' => true, 'data' => $user_accounts ]);
	}

	public function toggle($id, $status){
		$user_accounts = User::findOrFail($id);
		$user_accounts->is_ban = $status;
		if($user_accounts->save()){
			$title = ($status == 1)? 'Deactivated' : 'Activated';
			return response()->json(['status' => true, 'message' => 'User '.$title.' successfully!' ]);
		}
	}

}