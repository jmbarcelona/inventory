<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Customer;
use Validator;
use Auth;

class CustomerController extends Controller
{
	public function index(){
		return view('Customer.index');
	}

	public function list(){
		$customers = Customer::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $customers ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;						
		$validator = Validator::make($request->all(), [
			'customer_name' => 'required',
			'address' => 'required',
			'contact_number' => 'required',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			if(!empty($id)){
				$customers = Customer::where('id', $id)->update([
					'customer_name' => $request->get('customer_name'),
					'contact_person' => $request->get('contact_person'),
					'address' => $request->get('address'),
					'contact_number' => $request->get('contact_number'),
					'email' => $request->get('email'),
				]);
				if($customers){
					makeLog($user_id, 'Updated Customer', 'Updated Customer named '.$request->customer_name.' with the ID:'.$id);
					return response()->json(['status' => true, 'message' => 'customers saved successfully!']);
				}
			}else{
				$customers = Customer::create([
					'customer_name' => $request->get('customer_name'),
					'contact_person' => $request->get('contact_person'),
					'address' => $request->get('address'),
					'contact_number' => $request->get('contact_number'),
					'email' => $request->get('email'),
				]);
				if($customers){
					makeLog($user_id, 'Added Customer', 'Added Customer named '.$request->customer_name.' with the ID:'.$customers->id);
					return response()->json(['status' => true, 'message' => 'customers updated successfully!']);
				}
			}
		}
	}

	public function find($id){
		$customers = Customer::findOrFail($id);
		return response()->json(['status' => true, 'data' => $customers ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;								
		$customers = Customer::findOrFail($id);
		$customers->deleted_at = now();
		if($customers->save()){
			makeLog($user_id, 'Deleted Customer', 'Deleted Customer named '.$request->customer_name.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}