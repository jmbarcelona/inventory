<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Stock;
use App\Models\Material;
use App\Models\Product;
use App\Models\ProductDetail;
use Validator;
use Auth;

class StockController extends Controller
{
	public function index($type = ''){
		return view('StockFlow.index', compact('type'));
	}

	public function list($type = ''){
		$shown = ($type == 'products')? 'material_id' : 'product_id';
		$stocks = Stock::with('user')->whereNull($shown)->orderBy('id', 'desc')->get();
		return response()->json(['status' => true, 'data' => $stocks ]);
	}

	public function save(Request $request, $stock_id = ""){
		$user_id = Auth::user()->id;

		$qty_check = ['status' => true];


		$types = (!empty($request->get('product_id')))? 'products' : 'materials';
		$type_id = (!empty($request->get('product_id')))? $request->get('product_id') : $request->get('material_id');


		$validator = Validator::make($request->all(), [
			'qty' => 'required|numeric|gt:0',
			'status' => 'required',
			'note' => ($request->get('status') == 'stock_out')? 'required' : '',
		]);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{

			if ($request->get('status') == 'stock_out') {
				$qty_check = (!empty($request->get('product_id')))? $this->checkQtyProdcuts($request->get('product_id'), $request->get('qty')) : $this->checkQtyMaterials($request->get('material_id'), $request->get('qty'));
			}

			if ($qty_check['status'] == false) {
				$validator->errors()->add('qty', $qty_check['message']);
				return response()->json(['status' => false, 'error' => $validator->errors() ]);
			}else{

				$item_computation = $this->updateStocks($types, $type_id, $request->get('qty'), $request->get('status'));

				if ($item_computation['status'] === true) {
					$stocks = Stock::create([
						'product_id' => $request->get('product_id'),
						'material_id' => $request->get('material_id'),
						'qty' => $request->get('qty'),
						'status' => $request->get('status'),
						'note' => $request->get('note'),
						'user_id' => $user_id,
						'qty_at_that_time' => $item_computation['remaining'],
						'updated_qty' => $item_computation['updated'],
					]);

					if($stocks){
						return response()->json(['status' => true, 'message' => 'stocks updated successfully!']);
					}
				}else{
					return response()->json(['status' => false, 'message' => $item_computation['message'], 'details' => $item_computation['details']]);
				}
			}
		}
	}





	public function updateStocks($type, $id, $qty, $status){
		if ($type == 'products') {
			return $this->updateQtyProducts($id, $qty, $status);
		}elseif ($type == 'materials') {
			return $this->updateQtyMaterials($id, $qty, $status);
		}
	}

	public function updateQtyMaterials($id, $qty, $status){
		$materials = Material::find($id);
		$remaining = (!empty($materials->qty))? $materials->qty : 0;
		$updated = 0;
		if ($status == 'stock_in') {
			$updated = ($remaining + $qty);
		}elseif ($status == 'stock_out') {
			$updated = ($remaining - $qty);
		}

		$materials->qty = $updated;
		if ($materials->save()) {
			return ['status' => true, 'remaining' => $remaining, 'updated' => $updated];
		}
	}

	public function updateQtyProducts($id, $qty, $status){

		$checking_materials = $this->checkRawMaterials($id, $qty);

		if ($checking_materials['errors'] > 0) {
			return ['status' => false, 'message' => 'Not enought raw materials.', 'details' => $checking_materials['details']];
		}else{
			$product = Product::find($id);
			$remaining = (!empty($product->qty))? $product->qty : 0;
			$updated = 0;
			if ($status == 'stock_in') {
				$updated = ($remaining + $qty);
				$this->deductRawMaterials($id, $qty);
			}elseif ($status == 'stock_out') {
				$updated = ($remaining - $qty);
			}

			$product->qty = $updated;
			if ($product->save()) {
				return ['status' => true, 'remaining' => $remaining, 'updated' => $updated];
			}
		}

		
	}


	public function checkRawMaterials($product_id, $qty){
		$error = 0;
		$data = [];
		$details = ProductDetail::with(['material'])->where('product_id', $product_id)->get();
		foreach ($details as $detail) {

			$new_qty = ($detail->qty * $qty);
			$checker = Material::where('id', $detail->material_id)->where('qty', '>=', $new_qty);
			if ($checker->count() > 0) {
				$data[] = ['valid' => $detail->material->material_name.' matched the required quantity of materials for this product.'];
			}else{
				$data[] = ['invalid' => $detail->material->material_name.'  have only '.$detail->material->qty.' sotcks left.'];
				$error++;
			}
		}

		return ['errors' => $error, 'details' => $data];
	}


	public function deductRawMaterials($product_id, $qty){
		$user_id = Auth::user()->id;
		$error = 0;
		$data = [];
		$details = ProductDetail::with(['material'])->where('product_id', $product_id)->get();
		foreach ($details as $detail) {
			$new_qty = ($detail->qty * $qty);
			
			$checker = Material::where('id', $detail->material_id)->where('qty', '>=', $new_qty);
			if ($checker->count() > 0) {
				$material = $checker->first();

				$item_computation  = $this->updateStocks('materials', $material->id, $new_qty, 'stock_out');
				if ($item_computation['status'] === true) {
					Stock::create([
						'material_id' => $material->id,
						'qty' => $new_qty,
						'status' => 'stock_out',
						'note' => "Used for making product with the product ID ".$product_id,
						'user_id' => $user_id,
						'qty_at_that_time' => $item_computation['remaining'],
						'updated_qty' => $item_computation['updated'],
					]);
				}

			}
		}

		return ['errors' => $error, 'details' => $data];
	}


	public function checkQtyMaterials($id, $qty){
		$materials = Material::find($id);
		$data_qty = (!empty($materials->qty))? $materials->qty : 0;
		$qty_check = ($data_qty  - $qty);

		if ($qty_check <= 0) {
			return ['status' => false, 'message' => 'You can\'t perform this action. This item have only '.$data_qty.' left from the inventory.'];
		}else{
			return ['status' => true];
		}
	}

	public function checkQtyProdcuts($id, $qty){
		$products = Product::find($id);
		$data_qty = (!empty($products->qty))? $products->qty : 0;
		$qty_check = ($data_qty  - $qty);

		if ($qty_check <= 0) {
			return ['status' => false, 'message' => 'You can\'t perform this action. This item have only '.$data_qty.' left from the inventory.'];
		}else{
			return ['status' => true];
		}
	}


	public function find($stock_id){
		$stocks = Stock::findOrFail($stock_id);
		return response()->json(['status' => true, 'data' => $stocks ]);
	}

	public function findItems($id, $type){
		$data = [];
		$param = ($type == 'products')? 'product_id' : 'material_id';

		$data = Stock::with('user')->where($param, $id)->orderBy('id', 'desc')->get();

		return ['data' => $data];
	}


	public function findItemsCode($code, $type){

		if ($type == 'products') {
			$check_product = Product::where('product_code', $code);
			if ($check_product->count() > 0) {
				return response()->json(['status' => true, 'data' => $check_product->first()]);
			}else{
				return response()->json(['status' => false, 'message' => 'item code was not found!']);
			}	
		}else{
			$check_material = Material::where('item_code', $code);
			if ($check_material->count() > 0) {
				return response()->json(['status' => true, 'data' => $check_material->first()]);
			}else{
				return response()->json(['status' => false, 'message' => 'item code was not found!']);
			}	
		}
	}



	public function delete($stock_id){
		$stocks = Stock::findOrFail($stock_id);
		if($stocks->delete()){
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}