<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Models\ProductDetail;
use App\Models\Material;
use Validator;
use Str;
use Auth;

class ProductController extends Controller
{
	public function index(){
		return view('Product.index');
	}

	public function list(){
		$products = Product::whereNull('deleted_at')->get();
		return response()->json(['status' => true, 'data' => $products ]);
	}

	public function save(Request $request, $id = ""){
		$user_id = Auth::user()->id;										
		$rule2 = [];

		$rules = [
			'product_code' => 'required|unique:products,product_code,'.$id,
			'product_name' => 'required',
			'alert_qty' => 'required|numeric|min:1',
			'price' => 'required',
		];

		$rule2 = $this->validationProductDetails($request->get('details'));
		$validation_rules = array_merge($rules, $rule2['validation']);
		$validator = Validator::make($request->all(), $validation_rules, $rule2['rules']);

		if($validator->fails()){
			return response()->json(['status' => false, 'error' => $validator->errors() ]);
		}else{
			$data = [
				'product_code' => $request->get('product_code'),
				'product_name' => $request->get('product_name'),
				'description' => $request->get('description'),
				'price' => $request->get('price'),
				'alert_qty' => $request->get('alert_qty')
			];

			if ($request->has('image')) {
				$data['image'] = $request->file('image')->store('', 'products');
			}

			if(!empty($id)){
				$products = Product::where('id', $id)->update($data);
				if($products){
					makeLog($user_id, 'Updated Product', 'Updated Product named '.$request->product_name.' with the ID:'.$id);
					$this->saveProductDetails($request->get('details'), $id);
					return response()->json(['status' => true, 'message' => 'products saved successfully!']);
				}
			}else{
				$products = Product::create($data);
				if($products){
					makeLog($user_id, 'Added Product', 'Added Product named '.$request->product_name.' with the ID:'.$products->id);
					$this->saveProductDetails($request->get('details'), $products->id);
					return response()->json(['status' => true, 'message' => 'products updated successfully!']);
					// return response()->json(['status' => false, 'data' => $products->id]);
				}
			}
		}
	}


	public function validationProductDetails($details){
		$datas = [];
		$message_rules = [];

		if (!empty($details)) {
			foreach ($details as $key => $data) {
				if (empty($data['qty'])) {
					$datas['qty_'.$key] = 'required';
					$message_rules['qty_'.$key.'.required'] = 'The qty fields is required.';
				}

				if (empty($data['material_id'])) {
					$datas['material_id_'.$key] = 'required';
					$message_rules['material_id_'.$key.'.required'] = 'The material fields is required.';
				}
			}
		}

		return ['validation' => $datas, 'rules' => $message_rules];
	}

	public function saveProductDetails($details, $id){
		$clear = ProductDetail::where('product_id', $id)->delete();
		$counter = 0;
		foreach ($details as $key => $data) {
			ProductDetail::create(['product_id' => $id, 'material_id' => $data['material_id'], 'qty' => $data['qty']]);
			$counter++;
		}
		
		return $counter;
	}

	public function edit($id){
		$title = 'Edit Products';
		$products = Product::with(['product_details'])->findOrFail($id);		
		$materials = Material::get();
		return view('Product.add', compact('products', 'title', 'materials'));
	}

	public function add(){
		$title = 'Add Products';
		$materials = Material::get();
		return view('Product.add', compact('title', 'materials'));
	}

	public function find($id){
		$products = Product::findOrFail($id);
		return response()->json(['status' => true, 'data' => $products ]);
	}

	public function delete($id){
		$user_id = Auth::user()->id;										
		$products = Product::findOrFail($id);
		$products->deleted_at = now();
		if($products->save()){
			makeLog($user_id, 'Deleted Product', 'Deleted Product named '.$products->product_name.' with the ID:'.$id);
			return response()->json(['status' => true, 'message' => 'Record deleted successfully!' ]);
		}
	}

}