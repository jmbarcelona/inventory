<?php 
use App\Models\Product;
use App\Models\Material;
use App\Models\Customer;
use App\Models\Order;
use App\Models\Log;

function getProductCountQTY(){
	return number_format(Product::sum('qty'));
}

function getMaterialCountQTY(){
	return number_format(Material::sum('qty'));
}

function getProductCount(){
	return number_format(Product::count());
}

function getMaterialCount(){
	return number_format(Material::count());
}

function getCustomerCount(){
	return number_format(Customer::count());
}

function getCustomerOption(){
	return Customer::get();
}

function getOrderCountCompleted(){
	return number_format(Order::where('status', 1)->count());
}

function getOrderCount(){
	return number_format(Order::where('status', 0)->count());
}

function getAlertMaterials(){
	return Material::whereRaw('qty <=  alert_qty')->orWhere(function($q){
		$q->whereNull('alert_qty');
	})->get();
}

function getAlertProducts(){
	return Product::whereRaw('qty <=  alert_qty')->orWhere(function($q){
		$q->whereNull('alert_qty');
	})->get();
}

function getAlertMaterialsCheck(){
	return Material::whereRaw('qty <=  alert_qty')->orWhere(function($q){
		$q->whereNull('alert_qty');
	})->count();
}

function getAlertProductsCheck(){
	return Product::whereRaw('qty <=  alert_qty')->orWhere(function($q){
		$q->whereNull('alert_qty');
	})->count();
}

function makeLog($user_id, $title, $message){
	return Log::create(['user_id' => $user_id, 'title' => $title, 'message' => $message]);
}

function orderDashboard(){
	$pending  = [];
	$completed  = [];
	$year_today = date('Y');
	$years = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	$years_count = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
	foreach ($years_count as $year) {
		$completed[] = Order::whereNull('deleted_at')->whereYear('delivery_date', $year_today)->whereMonth('delivery_date', $year)->where('status', 1)->count();
		$pending[] = Order::whereNull('deleted_at')->whereYear('delivery_date', $year_today)->whereMonth('delivery_date', $year)->where('status', 0)->count();
	}

	return ['labels' => $years, 'pending' => $pending, 'completed' => $completed];

}

function getOrderSlug(){
	$max = Order::max('id');
	return 'OR-'.rand(10,99).rand(10,99).$max.rand(10,99).rand(10,99);
}