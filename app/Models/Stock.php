<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
	use HasFactory;

	protected $fillable = [
		'product_id',
		'material_id',
		'qty_at_that_time',
		'qty',
		'updated_qty',
		'status',
		'note',
		'user_id'
	];

	protected $appends = ['items'];

	public function user(){
		return $this->belongsTo('App\Models\User', 'user_id', 'id');
	}

	public function products(){
		return $this->belongsTo('App\Models\Product', 'product_id', 'id');
	}

	public function materials(){
		return $this->belongsTo('App\Models\Material', 'material_id', 'id');
	}

	public function getItemsAttribute($value){
		if (!empty($this->product_id)) {
			return ['name' => $this->products->product_name, 'code' => $this->products->product_code];
		}else{
			return ['name' => $this->materials->material_name, 'code' => $this->materials->item_code];
		}
	}

	public function getCreatedAtAttribute($value){
		return date('Y/m/d H:i:s A', strtotime($value));
	}
}