<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
	use HasFactory;

	protected $fillable = [
		'product_id',
		'material_id',
		'qty',
	];

	public function product(){
		return $this->belongsTo('App\Models\Product', 'product_id', 'id');
	}

	public function material(){
		return $this->belongsTo('App\Models\Material', 'material_id', 'id');
	}
}