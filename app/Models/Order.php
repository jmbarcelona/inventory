<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	use HasFactory;

	protected $fillable = [
		'customer_id',
		'delivery_date',
		'status',
		'total_amount',
		'note',
		'user_id',
		'last_edited',
		'total_amount_receive',
		'amount_changed',
		'order_tracking_no'
	];


	public function customer(){
		return $this->belongsTo('App\Models\Customer', 'customer_id', 'id');
	}

	public function items(){
		return $this->hasMany('App\Models\OrderDetail', 'order_id', 'id');
	}

}