<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DNS1D;

class Material extends Model
{
	use HasFactory;

	protected $fillable = [
		'item_code',
		'material_name',
		'category_id',
		'supplier_id',
		'brand_id',
		'price_per_kilo',
		'alert_qty'
	];


	public function brands(){
		return $this->belongsTo('App\Models\Brand', 'brand_id');
	}

	public function suppliers(){
		return $this->belongsTo('App\Models\Supplier', 'supplier_id');
	}

	public function categories(){
		return $this->belongsTo('App\Models\Category', 'category_id');
	}

	public function stock_flow(){
		return $this->hasMany('App\Models\Stock', 'material_id', 'id');
	}

}