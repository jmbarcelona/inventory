<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    use HasFactory;

    protected $fillable = [
    	'title',
    	'message',
    	'user_id'
    ];

    public function user(){
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function getCreatedAtAttribute($value){
		return date('Y/m/d H:i:s A', strtotime($value));
	}
}
