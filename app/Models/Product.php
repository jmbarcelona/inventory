<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DNS1D;
class Product extends Model
{
	use HasFactory;

	protected $fillable = [
		'image',
		'product_code',
		'product_name',
		'description',
		'qty',
		'alert_qty',
		'price',
	];

	protected $appends = ['logo'];

	public function getLogoAttribute(){
		if (!empty($this->image)) {
			return asset('products/'.$this->image);
		}else{
			return asset('img/default.jpg');
		}
	}

	public function product_details(){
		return $this->hasMany('App\Models\ProductDetail', 'product_id', 'id');
	}

	public function stock_flow(){
		return $this->hasMany('App\Models\Stock', 'product_id', 'id');
	}

}