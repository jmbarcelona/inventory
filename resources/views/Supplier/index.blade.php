@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Suppliers Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_suppliers();" class="btn btn-primary">Add Suppliers</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_suppliers" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="supplier_form" action="{{ route('suppliers.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_supplier_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_supplier_form_title">Add Suppliers</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Supplier</label>
							<input type="text" name="supplier" id="supplier" class="form-control " required />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="supplier_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
var tbl_suppliers;
show_suppliers();
function show_suppliers(){
	if(tbl_suppliers){
		tbl_suppliers.destroy();
	}
	tbl_suppliers = $('#tbl_suppliers').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('suppliers.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'supplier',
				"title": 'Supplier',
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_suppliers('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_suppliers('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					return newdata;
				}
			}
		]
	});
}


$("#supplier_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#supplier_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_suppliers();
				$('#modal_supplier_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('supplier_form', response.error);
				$('#supplier_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#supplier_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_suppliers(){
	$("#id").val('');
	$('#supplier').val('');
	$("#modal_supplier_form").modal('show');
}


function edit_suppliers(id){
	$.ajax({
		type:"GET",
		url:"{{ route('suppliers.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#supplier').val(response.data.supplier);
				$('#modal_supplier_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_suppliers(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete suppliers?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('suppliers.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_suppliers();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}


</script>
@endsection
