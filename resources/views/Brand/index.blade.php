@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Brands Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_brands();" class="btn btn-primary">Add Brands</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_brands" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="brand_form" action="{{ route('brands.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_brand_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_brand_form_title">Add Brands</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Brand</label>
							<input type="text" name="brand" id="brand" class="form-control form-control" required />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="brand_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
var tbl_brands;
show_brands();
function show_brands(){
	if(tbl_brands){
		tbl_brands.destroy();
	}
	tbl_brands = $('#tbl_brands').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('brands.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'brand',
				"title": 'Brand',
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_brands('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_brands('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					return newdata;
				}
			}
		]
	});
}


$("#brand_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#brand_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_brands();
				$('#modal_brand_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('brand_form', response.error);
				$('#brand_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#brand_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_brands(){
	$("#id").val('');
	$('#brand').val('');
	$("#modal_brand_form").modal('show');
}


function edit_brands(id){
	$.ajax({
		type:"GET",
		url:"{{ route('brands.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#brand').val(response.data.brand);
				$('#modal_brand_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_brands(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete brands?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('brands.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_brands();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
