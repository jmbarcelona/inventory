@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-lg-4 col-6">
			<div class="small-box bg-info">
				<div class="inner">
					<h3>{{getProductCount() }} <sup style="font-size: 15px">(QTY:{{ getProductCountQTY() }})</sup></h3>
					<p>Products</p>
				</div>
				<div class="icon">
					<i class="fas fa-dolly-flatbed"></i>
				</div>
				<a href="{{ route('products.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-4 col-6">
			<div class="small-box bg-success">
				<div class="inner">
					<h3>{{getMaterialCount() }} <sup style="font-size: 15px">(QTY:{{ getMaterialCountQTY() }})</sup></h3>
					<p>Raw Materials</p>
				</div>
				<div class="icon">
					<i class="far fa-dot-circle"></i>
				</div>
				<a href="{{ route('materials.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-4 col-6">
			<div class="small-box bg-warning">
				<div class="inner">
					<h3>{{getCustomerCount() }}</h3>
					<p>Customers</p>
				</div>
				<div class="icon">
					<i class="fas fa-people-carry"></i>
				</div>
				<a href="{{ route('customers.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-6 col-6">
			<div class="small-box bg-primary">
				<div class="inner">
					<h3>{{getOrderCountCompleted() }}</h3>
					<p>Completed Orders</p>
				</div>
				<div class="icon">
					<i class="fas fa-shipping-fast"></i>
				</div>
				<a href="{{ route('orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-lg-6 col-6">
			<div class="small-box bg-danger">
				<div class="inner">
					<h3>{{getOrderCount() }}</h3>
					<p>Pending Orders</p>
				</div>
				<div class="icon">
					<i class="fas fa-shipping-fast"></i>
				</div>
				<a href="{{ route('orders.index') }}" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
			</div>
		</div>
		<div class="col-md-12 mt-2">

			@if(getAlertMaterialsCheck() > 0 || getAlertProductsCheck() > 0)
				<div class="alert alert-info alert-dismissible animated slideInDown" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><i class="fas fa-lightbulb"></i> Decision Support</strong>, Better check your stocks regularly!
				</div>
			@else
				<div class="alert alert-success alert-dismissible animated slideInDown" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				   <strong><i class="fas fa-lightbulb"></i> Decision Support</strong>, Stocks are great today!
				</div>
			@endif


			@if(getAlertMaterialsCheck() > 0)
			<a href="{{ route('support.index') }}">
				<div class="alert alert-danger animated slideInDown alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><i class="fa fa-exclamation-circle"></i></strong> <b>Raw Materials</b> stocks are not looking good.
				</div>
			</a>
			@endif



			@if(getAlertProductsCheck() > 0)
			<a href="{{ route('support.index') }}">
				<div class="alert alert-danger animated slideInDown alert-dismissible" role="alert">
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				  <strong><i class="fa fa-exclamation-circle"></i></strong> <b>Products</b> stocks are not looking good.
				</div>
			</a>
				@endif
		</div>
		<div class="col-md-12">
			<div class="card card-info">
				<div class="card-header">
					<h3 class="card-title">Order Analytics - {{ date('Y') }}</h3>
					<div class="card-tools">
						<button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
					</div>
				</div>
				<div class="card-body">
					<div class="chart"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
						<canvas id="barChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('StockFlow.modal')
@endsection


@section('script')
@include('StockFlow.script')
<script src="{{ asset('adminLTE/plugins/chart.js/Chart.min.js') }}"></script>
<script>
	$("#table_materials").dataTable();
	$("#table_products").dataTable();
</script>

<script type="text/javascript">
	$(function () {
    /* ChartJS
     * -------
     * Here we will create a few charts using ChartJS
     */

    //--------------
    //- AREA CHART -
    //--------------

    // Get context with jQuery - using jQuery's .get() method.

    var areaChartData = {
      labels  : JSON.parse('{!! json_encode(orderDashboard()['labels']) !!}'),
      datasets: [
        {
          label               : 'Completed',
          backgroundColor     : 'rgba(60,141,188,0.9)',
          borderColor         : 'rgba(60,141,188,0.8)',
          pointRadius          : false,
          pointColor          : '#3b8bba',
          pointStrokeColor    : 'rgba(60,141,188,1)',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(60,141,188,1)',
          data                : JSON.parse('{!! json_encode(orderDashboard()['completed']) !!}')
        },
        {
          label               : 'Pending',
          backgroundColor     : 'rgba(210, 214, 222, 1)',
          borderColor         : 'rgba(210, 214, 222, 1)',
          pointRadius         : false,
          pointColor          : 'rgba(210, 214, 222, 1)',
          pointStrokeColor    : '#c1c7d1',
          pointHighlightFill  : '#fff',
          pointHighlightStroke: 'rgba(220,220,220,1)',
          data                : JSON.parse('{!! json_encode(orderDashboard()['pending']) !!}')
        },
      ]
    }

    //-------------
    //- BAR CHART -
    //-------------
    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = $.extend(true, {}, areaChartData)
    var temp0 = areaChartData.datasets[0]
    var temp1 = areaChartData.datasets[1]
    barChartData.datasets[0] = temp1
    barChartData.datasets[1] = temp0

    var barChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      datasetFill             : false
    }

    new Chart(barChartCanvas, {
      type: 'bar',
      data: barChartData,
      options: barChartOptions
    })

    //---------------------
    //- STACKED BAR CHART -
    //---------------------
    var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
    var stackedBarChartData = $.extend(true, {}, barChartData)

    var stackedBarChartOptions = {
      responsive              : true,
      maintainAspectRatio     : false,
      scales: {
        xAxes: [{
          stacked: true,
          precision: 0
        }],
        yAxes: [{
          	stacked: true,
        }]
      }
    }

    new Chart(stackedBarChartCanvas, {
      type: 'bar',
      data: stackedBarChartData,
      options: stackedBarChartOptions
    })
  })
</script>
@endsection
