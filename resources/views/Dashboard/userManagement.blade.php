@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')

this is user management
<div class="row">
	<div class="col-lg-12 col-md-12 col-12">
		<div class="border-bottom pb-4 mb-4 d-md-flex justify-content-between align-items-center">
			<div class="mb-3 mb-md-0">
				<h1 class="mb-1 h2 fw-bold">Users</h1>
				@include('Layout.bread', ['data' => [ 'Users' => url()->current() ] ])
			</div>
			<div class="d-flex">
				  <button class="btn btn-primary btn-shadow" onclick="add_Users();"><i class="fa fa-plus"></i> Add Users</button>
			</div>
		</div>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<div class="table-responsive">
			<table class="table table-bordered dt-responsive nowrap" id="tbl_Users" style="width: 100%;"></table>
		</div>
	</div>
</div>

<form class="needs-validation" id="user_reg_form" action="{{ url('Users/add') }}" novalidate>
	<div class="modal fade" role="dialog" id="modal_Users" data-bs-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title" id="modal_title_Users">
					Add Users
				</div>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
			<div class="modal-body">
				<div class="row">
					<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control" required>
					<div class="position-relative mb-2 col-sm-12">
						<label>User Id </label>
						<input type="hidden" id="user_id" name="user_id" placeholder="" class="form-control " required>
						<div class="invalid-feedback" id="err_user_id"></div>
					</div>
					<div class="position-relative mb-2 col-sm-6">
						<label>Firstname </label>
						<input type="text" id="Firstname" name="Firstname" placeholder="Enter Firstname" class="form-control " required>
						<div class="invalid-feedback" id="err_Firstname"></div>
					</div>
					<div class="position-relative mb-2 col-sm-6">
						<label>Middlename </label>
						<input type="text" id="Middlename" name="Middlename" placeholder="Enter Middlename" class="form-control " required>
						<div class="invalid-feedback" id="err_Middlename"></div>
					</div>
					<div class="position-relative mb-2 col-sm-12">
						<label>Lastname </label>
						<input type="text" id="Lastname" name="Lastname" placeholder="Enter Lastname" class="form-control " required>
						<div class="invalid-feedback" id="err_Lastname"></div>
					</div>
					<div class="position-relative mb-2 col-sm-6">
						<label>Gender </label>
						<select id="gender" name="gender" class="form-control ">
							<option value="Male">Male</option>
							<option value="Female">Female</option>
							<option value="Other">Other</option>
						</select>
						<div class="invalid-feedback" id="err_gender"></div>
					</div>
						<div class="position-relative mb-2 col-sm-6">
						<label>Contact </label>
						<input type="text" id="contact" name="contact" placeholder="Enter contact number here" class="form-control " required>
						<div class="invalid-feedback" id="err_contact"></div>
					</div>
					<div class="position-relative mb-2 col-sm-6">
						<label>Province </label>
						<input type="text" id="province" name="province" placeholder="Enter province here" class="form-control " required>
						<div class="invalid-feedback" id="err_province"></div>
					</div>
					<div class="position-relative mb-2 col-sm-6">
						<label>City </label>
						<input type="text" id="city" name="city" placeholder="Enter city here" class="form-control " required>
						<div class="invalid-feedback" id="err_city"></div>
					</div>
					<div class="position-relative mb-2 col-sm-12">
						<label>Brgy </label>
						<input type="text" id="brgy" name="brgy" placeholder="Enter barangay here" class="form-control " required>
						<div class="invalid-feedback" id="err_brgy"></div>
					</div>
					<div class="position-relative mb-2 col-sm-12">
						<label>Address </label>
						<input type="text" id="address" name="address" placeholder="Enter address here" class="form-control " required>
						<div class="invalid-feedback" id="err_address"></div>
					</div>
					<div class="position-relative mb-2 col-sm-12">
						<label>Email </label>
						<input type="email" id="Email" name="Email" placeholder="Enter email here" class="form-control " required>
						<div class="invalid-feedback" id="err_Email"></div>
					</div>
					<div class="position-relative mb-2 col-sm-12">
						<label>Password </label>
						<input type="password" id="Password" name="Password" placeholder="Enter password" class="form-control " required>
						<div class="invalid-feedback" id="err_Password"></div>
					</div>
				</div>
			</div>
			<div class="modal-footer d-block">
				<div class="row">
					<div class="col-sm-6 col-12">
						<button class="btn btn-dark col-sm-12 col-12" data-bs-dismiss="modal" type="button">Cancel</button>
					</div>
					<div class="col-sm-6 col-12">
						<button class="btn btn-success col-sm-12 col-12" id="btn_submit_Users" type="submit">Save</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</form>
@endsection
@section('script')
<!-- Javascript Function-->
<script>
show_Users();
	var tbl_Users;
	function show_Users(){
		if (tbl_Users) {
			tbl_Users.destroy();
		}
		var url = main_path + 'Users/list';
		tbl_Users = $('#tbl_Users').DataTable({
		pageLength: 10,
		responsive: true,
		ajax: url,
		deferRender: true,
		language: {
		"emptyTable": "No data available"
	},
		columns: [{
		className: '',
		"data": "user_id",
		"title": "User id",
	},{
		className: '',
		"data": "Firstname",
		"title": "Firstname",
	},{
		className: '',
		"data": "Middlename",
		"title": "Middlename",
	},{
		className: '',
		"data": "Lastname",
		"title": "Lastname",
	},{
		className: '',
		"data": "gender",
		"title": "Gender",
	},{
		className: '',
		"data": "contact",
		"title": "Contact",
	},{
		className: '',
		"data": "province",
		"title": "Province",
	},{
		className: '',
		"data": "city",
		"title": "City",
	},{
		className: '',
		"data": "brgy",
		"title": "Brgy",
	},{
		className: '',
		"data": "address",
		"title": "Address",
	},{
		className: '',
		"data": "Email",
		"title": "Email",
	},{
		className: '',
		"data": "Password",
		"title": "Password",
	},{
		className: 'width-option-1 text-center',
		"data": "user_id",
		"orderable": false,
		"title": "Options",
			"render": function(data, type, row, meta){
				newdata = '';
				newdata += '<button class="btn btn-success btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="edit_Users(this)" type="button"><i class="fa fa-edit"></i></button> ';
				newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" data-id=\' '+row.user_id+'\' onclick="delete_Users(this)" type="button"><i class="fa fa-trash"></i></button>';
				return newdata;
			}
		}
	]
	});
	}

	$("#user_reg_form").on('submit', function(e){
		var url = $(this).attr('action');
		var mydata = $(this).serialize();
		e.stopPropagation();
		e.preventDefault(e);

		$.ajax({
			type:"POST",
			url:url,
			data:mydata,
			cache:false,
			beforeSend:function(){
					//<!-- your before success function -->
					$('#btn_submit_Users').prop('disabled', true);
					$('#btn_submit_Users').text('Please wait...');
			},
			success:function(response){
					//console.log(response)
				if(response.status == true){
					console.log(response)
					swal("Success", response.message, "success");
					showValidator(response.error,'user_reg_form');
					show_Users();
					$('#modal_Users').modal('hide');
				}else{
					//<!-- your error message or action here! -->
					showValidator(response.error,'user_reg_form');
				}
				$('#btn_submit_Users').prop('disabled', false);
				$('#btn_submit_Users').text('Save');
			},
			error:function(error){
				console.log(error)
			}
		});
	});

	function delete_Users(_this){
		var id = $(_this).attr('data-id');
		var url =  main_path + 'Users/delete/' + id;
			swal({
				title: "Are you sure?",
				text: "Do you want to delete this Users?",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes",
				closeOnConfirm: false
			},
			function(){
				$.ajax({
				type:"delete",
				url:url,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					show_Users();
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}

	function add_Users(){
		$('#modal_title_Users').text('Add Users');
		$('#user_id').val('');
		showValidator([],'user_reg_form');
		$('#user_reg_form').removeClass('was-validated');
		$('#user_id').val('');
		$('#Firstname').val('');
		$('#Middlename').val('');
		$('#Lastname').val('');
		$('#gender').val('');
		$('#contact').val('');
		$('#province').val('');
		$('#city').val('');
		$('#brgy').val('');
		$('#address').val('');
		$('#Email').val('');
		$('#Password').val('');
		$('#modal_Users').modal('show');
	}

	function edit_Users(_this){
		$('#modal_title_Users').text('Edit Users');
		var id = $(_this).attr('data-id');
		var url =  main_path + 'Users/find/' + id;
		$.ajax({
		type:"get",
		url:url,
		data:{},
		dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			let data = response.data;
			$('#user_id').val(data.user_id);
			$('#user_id').val(data.user_id);
			$('#Firstname').val(data.Firstname);
			$('#Middlename').val(data.Middlename);
			$('#Lastname').val(data.Lastname);
			$('#gender').val(data.gender);
			$('#contact').val(data.contact);
			$('#province').val(data.province);
			$('#city').val(data.city);
			$('#brgy').val(data.brgy);
			$('#address').val(data.address);
			$('#Email').val(data.Email);
			$('#Password').val(data.Password);
			$('#modal_Users').modal('show');
		},
		error: function(error){
			console.log(error);
		}
		});
	}
</script>
@endsection
