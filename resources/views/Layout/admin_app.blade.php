<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition sidebar-mini layout-fixed">
		<div class="wrapper">
			@include('Layout.admin_nav')
			@include('Layout.sidebar')
		 	@yield('breadcrumbs')
		 	<div class="content-wrapper">
		 		<section class="content">
					 <div class="container-fluid">
							@yield('content')
					</div>
				 </section>
		 	</div>
		</div>

	<div class="modal fade" role="dialog" id="previewer_modal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="modal-title">
						Preview
					</div>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<center>
						<img src="" id="previewer" alt="preview_image" class="img-fluid img-thumbnail"><br>
						<span id="previewer_text"></span>
					</center>
				</div>
				<div class="modal-footer">
					
				</div>
			</div>
		</div>
	</div>
	</body>
	@include('Layout.footer')
	@yield('script')
	<script type="text/javascript">
		function show_full_image(_this, text = ''){
			let url = $(_this).attr('src');
			$("#previewer").attr('src', url);
			$("#previewer_text").text(text);
			$("#previewer_modal").modal('show');
		}
	</script>
</html>