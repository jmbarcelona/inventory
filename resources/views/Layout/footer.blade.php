<form id="price_scanner_form" class="needs-validation" action="{{ route('stocks.find.items.code') }}" >
	<div class="modal fade" role="dialog" id="price_scanner_modal" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <div class="modal-title text-capitalize">
	       		Product Price Check
	        </div>
	        <button class="close" onclick="$('#price_scanner_modal').modal('hide');">&times;</button>
	      </div>
	      <div class="modal-body">
	      	<div class="text-center">
	      		<h2>Please Scan the item code</h2>
	      		<img src="{{ asset('img/scan.gif') }}" class="img-fluid" alt="">
	      	</div>
	        <input type="text" id="price_scanned_code" name="price_scanned_code" class="form-control" required style="opacity: 0;">
	      </div>
	    </div>
	  </div>
	</div>
</form>

 <div class="modal fade" role="dialog" id="modal_price_check">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title" >
        Price Check
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="text-center">
        	<img src="" alt="" class="img-fluid" id="logo_price_check">
        	<h3 class="m-2" id="price_product_name"></h3>
        	<h1 class="m-2" id="price_product_price"></h1>
        	<h1 id="price_product_qty"></h1>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- jQuery -->
<script src="{{ asset('adminLTE/plugins/jquery/jquery.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{asset('js/ajaxsetup.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- DataTables  & Plugins -->
<script src="{{ asset('adminLTE/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/jszip/jszip.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/pdfmake/pdfmake.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/pdfmake/vfs_fonts.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('adminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js') }}"></script>
<!-- adminLTE App -->
<script src="{{ asset('adminLTE/dist/js/adminlte.min.js') }}"></script>
<!-- JS -->
<script type="text/javascript" src="{{asset('js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/validation.js')}}"></script>
<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('js/barcode.js')}}"></script>
<script type="text/javascript">
	function open_price_scanner(){
		$("#price_scanner_modal").modal('show');
		$("#price_scanned_code").val('');
		setTimeout(function(){
			$("#price_scanned_code").focus();
		},1000);
	}


	$("#price_scanner_form").on('submit', function(event){
		event.preventDefault();
		$("#scanner_modal").modal('show');
		let type_scanned = 'products';
		let scanned_code = $("#price_scanned_code").val();
		let url = $(this).attr('action')+'/'+scanned_code+'/'+type_scanned;
		// alert(url);
		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      console.log(response);

		     if (response.status == true) {
		     	$("#price_product_name").text(response.data.product_name);
		     	$("#price_product_price").text(money.format(response.data.price));
		     	$("#price_product_qty").text('Stocks:'+response.data.qty);
		     	$("#logo_price_check").attr('src', response.data.logo);
		     	$("#modal_price_check").modal('show');

		     	setTimeout(function(){
			     	$("#modal_price_check").modal('hide');
		     	}, 8000);
		     }else{
		        swal("Error", response.message, "error");
		     }
		    },
		    error: function(error){
  		        swal("Error", "Please contact system administrator!", "error");
		    }
		  });
	});
</script>