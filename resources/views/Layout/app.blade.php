<!DOCTYPE html>
<html>
	<head>
		<title>@yield('title')</title>
		@include('Layout.header')
		@yield('css')
	</head>
	<body class="hold-transition">
		<div class="">
			<div class="bg-white">
			 	@yield('breadcrumbs')
			 	 <section class="content">
					 <div class="container-fluid">
						@yield('content')
					</div>
				 </section>
			</div>
		</div>
	</body>
	@include('Layout.footer')
	@yield('script')
</html>