<nav class="main-header navbar navbar-expand-lg bg-dark navbar-dark">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
         <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item dropdown d-none">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Online Registrations
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">Business Permit</a>
          <a class="dropdown-item" href="#">Cenomar</a>
          <a class="dropdown-item" href="#">Birth Certificate</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">More</a>
        </div>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <a  href="{{ route('logout') }}" class="btn btn-danger my-2 my-sm-0">Logout</a>
    </div>
  </div>
</nav>