<aside class="main-sidebar sidebar-dark-dark bg-dark elevation-4">
  <!-- Brand Logo -->
   <a class="brand-link" style="border-color: #a4b0d6;">
      <img src="{{ asset('img/logo.png') }}" alt="Logo IMG" class="brand-image" >
      <span class="brand-text font-weight-light">{{Auth::user()->name}}</span>
    </a>
  
  <!-- Sidebar -->
  <div class="sidebar p-0 mt-2">
    <!-- Sidebar user panel (optional) -->
    <!-- Sidebar Menu -->
    <nav class="sidebar side-item">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <!-- Sample Multi level -->
         <li class="nav-item ">
          <a href="{{ url('dashboard')}}" class="nav-link {{ (request()->is('dashboard'))? 'active' : '' }}">
            <i class="nav-icon fas fa-chart-line"></i>
            <p>
              Dashboard
            </p>
          </a>
        </li>
        <li class="nav-item {{ (request()->is('materials') || request()->is('brands') || request()->is('categories') || request()->is('suppliers') || request()->is('products'))? 'menu-is-opening menu-open': '' }}">
          <a href="#" class="nav-link {{ (request()->is('materials') || request()->is('brands') || request()->is('categories') || request()->is('suppliers') || request()->is('products'))? 'active': '' }}">
            <i class="nav-icon fa fa-warehouse"></i>
            <p>
              Inventory
                <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: {{ ( request()->is('products/*') || request()->is('materials') || request()->is('brands') || request()->is('categories') || request()->is('suppliers') || request()->is('products'))? 'block': 'none' }};">
            <li class="nav-item">
              <a href="{{ route('products.index') }}" class="nav-link {{ (request()->is('products') || request()->is('products/add'))? 'active' : '' }}">
                <i class="fas fa-dolly-flatbed nav-icon"></i>
                <p>Products</p>
              </a>
            </li>
            <li class="nav-item  {{ (request()->is('materials') || request()->is('brands') || request()->is('categories') || request()->is('suppliers'))? 'menu-is-opening menu-open': '' }}">
              <a href="#" class="nav-link {{ (request()->is('materials') || request()->is('brands') || request()->is('categories') || request()->is('suppliers'))? 'active' : '' }}">
                <i class="fa fa-boxes nav-icon"></i>
                <p>
                  Raw Materials
                    <i class="right fas fa-angle-left"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                 <li class="nav-item">
                  <a href="{{ route('materials.index') }}" class="nav-link {{ (request()->is('materials'))? 'active' : '' }}">
                    <i class="far fa-dot-circle nav-icon"></i>
                    <p>Materials</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('brands.index') }}" class="nav-link {{ (request()->is('brands'))? 'active' : '' }}">
                    <i class="fa fa-tag nav-icon"></i>
                    <p>Brands</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('categories.index') }}" class="nav-link {{ (request()->is('categories'))? 'active' : '' }}">
                    <i class="fab fa-microsoft nav-icon"></i>
                    <p>Categories</p>
                  </a>
                </li>
                <li class="nav-item">
                  <a href="{{ route('suppliers.index') }}" class="nav-link {{ (request()->is('suppliers'))? 'active' : '' }}">
                    <i class="fas fa-parachute-box nav-icon"></i>
                    <p>Supplier</p>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
         <li class="nav-item {{ (request()->is('stocks/materials') || request()->is('stocks/products'))? 'menu-is-opening menu-open': '' }}">
          <a href="#" class="nav-link {{ (request()->is('stocks/materials') || request()->is('stocks/products'))? 'active': '' }}">
            <i class="nav-icon fa fa-list-alt"></i>
            <p>
              Stock Flow
                <i class="right fas fa-angle-left"></i>         
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item ">
              <a href="{{ route('stocks.index')}}/materials" class="nav-link {{ (request()->is('stocks/materials'))? 'active' : '' }}">
                <i class="far fa-dot-circle nav-icon"></i>

                <p>
                  Materials Stock Flow
                </p>
              </a>
            </li>
            <li class="nav-item ">
              <a href="{{ route('stocks.index')}}/products" class="nav-link {{ (request()->is('stocks/products'))? 'active' : '' }}">
                <i class="fas fa-dolly-flatbed nav-icon"></i>                
                <p>
                  Products Stock Flow
                </p>
              </a>
            </li>
          </ul>
        </li>        
        <li class="nav-item ">
          <a href="{{ route('orders.index')}}" class="nav-link {{ (request()->is('orders'))? 'active' : '' }}">
            <i class="nav-icon fas fa-shipping-fast"></i>
            <p>
              Orders
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('customers.index')}}" class="nav-link {{ (request()->is('customers'))? 'active' : '' }}">
            <i class="nav-icon fas fa-people-carry"></i>
            <p>
              Customers
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('user_accounts.index')}}" class="nav-link {{ (request()->is('user-accounts'))? 'active' : '' }}">
            <i class="nav-icon fas fa-user-cog"></i>
            <p>
              User Management
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="{{ route('logs.index')}}" class="nav-link {{ (request()->is('logs'))? 'active' : '' }}">
            <i class="nav-icon fa fa-list"></i>
            <p>
              Activity Logs
            </p>
          </a>
        </li>
        <li class="nav-item ">
          <a href="javascript:;" onclick="open_price_scanner();" class="nav-link">
            <i class="nav-icon fas fa-barcode"></i>
            <p>
              Price Check
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
