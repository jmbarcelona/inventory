<nav class="navbar navbar-expand-lg bg-dark navbar-dark">
    <a class="navbar-brand" href="{{ route('home') }}"><img src="{{ asset('img/logo.png') }}" class="img-fluid" width="30" alt="Logo here">Inventory</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">News and Updates</a>
      </li>
    </ul>
    <div class="form-inline my-2 my-lg-0">
      <a  href="{{ route('login') }}" class="btn btn-primary my-2 my-sm-0" type="submit">Login</a>
    </div>
  </div>
</nav>