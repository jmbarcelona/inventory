@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Categories Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_categories();" class="btn btn-primary">Add Categories</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_categories" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="category_form" action="{{ route('categories.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_category_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_category_form_title">Add Categories</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Category</label>
							<input type="text" name="category" id="category" class="form-control " required />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="category_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
var tbl_categories;
show_categories();
function show_categories(){
	if(tbl_categories){
		tbl_categories.destroy();
	}
	tbl_categories = $('#tbl_categories').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('categories.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'category',
				"title": 'Category',
			},
			{
				className: 'width-option-1 text-center',
				width: '25%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_categories('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_categories('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					return newdata;
				}
			}
		]
	});
}


$("#category_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#category_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_categories();
				$('#modal_category_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('category_form', response.error);
				$('#category_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#category_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_categories(){
	$('#id').val('');
	$('#category').val('');
	$("#modal_category_form").modal('show');
}


function edit_categories(id){
	$.ajax({
		type:"GET",
		url:"{{ route('categories.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#category').val(response.data.category);
				$('#modal_category_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_categories(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete categories?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('categories.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_categories();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}


</script>
@endsection
