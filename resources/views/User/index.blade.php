@extends('Layout.admin_app')
@section('title', 'User Account Management')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>User accounts Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_user_accounts();" class="btn btn-primary">Add User accounts</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_user_accounts" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="user_account_form" action="{{ route('user_accounts.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_user_account_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_user_account_form_title">Add User accounts</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Name</label>
							<input type="text" name="name" id="name" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Gender</label>
							<select name="gender" id="gender" class="form-control " required>
								<option value="" selected>Select Gender</option>
								<option value="male">Male</option>
								<option value="female">Female</option>
							</select>
						</div>
						<div class="col-md-12">
							<label>Email address</label>
							<input type="email" name="email_address" id="email_address" class="form-control " required />
						</div>
						<div class="col-md-12 hidden">
							<label>Password</label>
							<input type="password" name="password" id="password" class="form-control " required />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="user_account_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
var tbl_user_accounts;
show_user_accounts();
function show_user_accounts(){
	if(tbl_user_accounts){
		tbl_user_accounts.destroy();
	}
	tbl_samples = $('#tbl_user_accounts').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('user_accounts.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'name',
				"title": 'Name',
			},
			{
				className: 'text-capitalize',
				"data": 'gender',
				"title": 'Gender',
			},
			{
				className: '',
				"data": 'email_address',
				"title": 'Email address',
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_user_accounts('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					if (row.is_ban == 1) {
						newdata += ' <button class="btn btn-info btn-sm font-base mt-1" title="Activate User" onclick="delete_user_accounts('+row.id+', 0);" type="button"><i class="fa fa-unlock"></i></button>';
					}else{
						newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" title="Deactivate User" onclick="delete_user_accounts('+row.id+', 1);" type="button"><i class="fa fa-user-lock"></i></button>';
					}
					return newdata;
				}
			}
		]
	});
}


$("#user_account_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#user_account_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_user_accounts();
				$('#modal_user_account_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('user_account_form', response.error);
				$('#user_account_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#user_account_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_user_accounts(){
	$("#id").val('');
	$('#name').val('');
	$('#gender').val('');
	$('#email_address').val('');
	$('#password').val('');
	$('.hidden').show();
	$("#modal_user_account_form").modal('show');
}


function edit_user_accounts(id){
	$.ajax({
		type:"GET",
		url:"{{ route('user_accounts.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#name').val(response.data.name);
				$('#gender').val(response.data.gender);
				$('#email_address').val(response.data.email_address);
				$('.hidden').hide();
				$('#modal_user_account_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_user_accounts(id, status){
	let title = (status == 1)? 'deactivate' : 'activate';

	swal({
		title: "Are you sure?",
		text: "Do you want to "+title+" this user account?",
		type: "info",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"get",
			url:"{{ route('user_accounts.toggle') }}/"+id+'/'+status,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_user_accounts();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
