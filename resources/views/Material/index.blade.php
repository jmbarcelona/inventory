@extends('Layout.admin_app')
@section('title', 'Materials')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Materials Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_materials();" class="btn btn-primary">Add Materials</button>
		<button type="button" onclick="$('.btn_print').click();" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
		<button type="button" onclick="$('.excelButton').click();" class="btn btn-success"><i class="fa fa-file-excel"></i> Export Excel</button>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_materials" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="material_form" action="{{ route('materials.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_material_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_material_form_title">Add Materials</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Item Code <small>Enter item code or use the scanner to get the code</small></label>
							<input type="text" name="item_code" id="item_code" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Material name</label>
							<input type="text" name="material_name" id="material_name" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Brands</label>
							<select name="brand" id="brand" class="form-control " required>
								<option value="" selected>Select Brand</option>
								@foreach($brand as $brands)
								<option value="{{ $brands->id }}">{{ $brands->brand }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12">
							<label>Categories</label>
							<select name="category" id="category" class="form-control " required>
								<option value="" selected>Select Category</option>
								@foreach($category as $categories)
								<option value="{{ $categories->id }}">{{ $categories->category }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12">
							<label>Supplier</label>
							<select name="supplier" id="supplier" class="form-control " required>
								<option value="" selected>Select Supplier</option>
								@foreach($supplier as $suppliers)
								<option value="{{ $suppliers->id }}">{{ $suppliers->supplier }}</option>
								@endforeach
							</select>
						</div>
						<div class="col-md-12">
							<label>Price per kilo</label>
							<input type="number" name="price_per_kilo" id="price_per_kilo" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Stock Alert <br><small class="text-info"><b> The System will alert you if the stocks is less than/qual to this value.</b></small></label>
							<input type="number" name="alert_qty" id="alert_qty" class="form-control form-control" required value="{{ $products->alert_qty ?? '' }}"/>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="material_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>

@include('StockFlow.modal')

@endsection


@section('script')
@include('StockFlow.script')

<!-- Javascript Function-->
<script>
var tbl_materials;
show_materials();
function show_materials(){
	if(tbl_materials){
		tbl_materials.destroy();
	}
	tbl_materials = $('#tbl_materials').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {extend : 'print', 
            className: 'btn_print d-none',
             customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="{{ asset('img/logo.png') }}"/><div class="h3">Ace Plastic</div></center>'
                        );
                },
                exportOptions: {
                        stripHtml : false,
                        columns: [0, 2, 3, 4, 5, 6, 7]
                }
		},
            { 
            	extend: 'excel', 
           		className: 'excelButton d-none',
           		exportOptions: {
                        columns: [0, 2, 3, 4, 5, 6, 7]
                } 
            }
        ],
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('materials.list') }}",
		deferRender: true,
		columns: [
			{
				className: 'd-none',
				"data": 'item_code',
				"title": 'Barcode',
			},{
				width: 90,
				className: 'text-center hide-print',
				"data": 'item_code',
				"title": 'Barcode',
				"render":function(data, type, row, meta){
					return '<svg class="qrcodes d-none" id="qrcode_'+data+'" data-value="'+data+'"></svg> <img id="img_qrcode_'+data+'">';
				}
			},
			{
				className: '',
				"data": 'material_name',
				"title": 'Material name',
			},
			{
				className: '',
				"data": 'brands.brand',
				"title": 'Brand',
			},
			{
				className: '',
				"data": 'suppliers.supplier',
				"title": 'Supplier',
			},
			{
				className: '',
				"data": 'categories.category',
				"title": 'Category',
			},
			{
				className: 'text-center',
				"data": 'qty',
				"title": 'QTY',
				"render":function(data, type, row, meta){
					if(data == '' || data == 0 || data == null){
						return '0';
					}else{
						return data;
					}
				}
			},
			{
				className: 'text-right',
				"data": 'price_per_kilo',
				"title": 'Price per kilo',
				"render": function(data){
					return money.format(data);
				}
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-info btn-sm font-base mt-1"  onclick="view_stocks('+row.id+', \'materials\', \''+row.material_name+'\')" type="button"><i class="fa fa-list"></i></button> ';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_materials('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_materials('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					newdata += ' <button class="btn btn-primary btn-sm font-base mt-1"  onclick="add_stocks('+row.id+', \'materials\', \''+row.material_name+'\', \'#'+row.item_code+'\')" type="button"><i class="fas fa-box-tissue"></i></button>';
					return newdata;
				}
			}
		]
	});

	setTimeout(function(){
		qrcodeRender();
	},1500);
}



$("#material_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#material_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_materials();
				$('#modal_material_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('material_form', response.error);
				$('#material_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#material_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_materials(){
	$("#id").val('');
	$('#item_code').val('');
	$('#material_name').val('');
	$('#category').val('');
	$('#supplier').val('');
	$('#brand').val('');
	$('#price_per_kilo').val('');
	$("#modal_material_form").modal('show');
}


function edit_materials(id){
	$.ajax({
		type:"GET",
		url:"{{ route('materials.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#item_code').val(response.data.item_code);
				$('#material_name').val(response.data.material_name);
				$('#category').val(response.data.category_id);
				$('#supplier').val(response.data.supplier_id);
				$('#brand').val(response.data.brand_id);
				$('#price_per_kilo').val(response.data.price_per_kilo);
				$("#alert_qty").val(response.data.alert_qty);
				$('#modal_material_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_materials(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete materials?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('materials.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_materials();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
