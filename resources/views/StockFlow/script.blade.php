<script type="text/javascript">
	var tbl_stocks;
	function show_stocks(type){
		if(tbl_stocks){
			tbl_stocks.destroy();
		}
		tbl_samples = $('#tbl_stocks').DataTable({
			destroy: true,
			pageLength: 10,
			responsive: true,
			ajax: "{{ route('stocks.list') }}/"+type,
			deferRender: true,
			columns: [
				{
					className: 'text-center',
					"data": 'items.code',
					"title": 'Item Code',
				},
				{
					className: 'text-center',
					"data": 'items.name',
					"title": 'Item Name',
				},
				{
					className: 'text-center',
					"data": 'qty_at_that_time',
					"title": 'Remaining Quantity',
					"render":function(data){
						return '<b>'+number_format.format(data)+'</b>';
					}
				},
				{
					className: 'text-center',
					"data": 'qty',
					"title": 'Added/Deducted QTY',
					"render": function(data, type, row, meta){
						if(row.status == 'stock_in'){
							return '<span class="text-success"><b>+'+number_format.format(data)+'</b></span>';
						}else{
							return '<span class="text-danger"><b>-'+number_format.format(data)+'</b></span>';

						}
					}
				},
				{
					className: 'text-right',
					"data": 'updated_qty',
					"title": 'Updated Qty',
					"render": function(data, type, row, meta){
						if(row.status == 'stock_in'){
							return '<span class="text-success h4"><b>'+number_format.format(data)+'</b></span>';
						}else{
							return '<span class="text-danger h4"><b>'+number_format.format(data)+'</b></span>';

						}
					}
				},
				{
					className: 'text-center',
					"width": '15',
					"data": 'status',
					"title": 'Status',
					"render":function(data){
						if(data == 'stock_in'){
							return '<span class="badge badge-success py-3 col-md-12">Stocked In</span>';
						}else{
							return '<span class="badge badge-danger py-3 col-md-12">Stocked Out</span>';
						}
					}
				},
				{
					className: 'text-center',
					"data": 'user.name',
					"title": 'Authorized By',
				},
				{
					className: 'text-center',
					"data": 'created_at',
					"title": 'Date Entry',
				},
				{
					className: 'text-center',
					"data": 'note',
					"title": 'Note',
					"render":function(data){
						let dt = (data === null || data === '')? 'Note is empty!' : data;
						return '<btn class="btn btn-info" onclick="swal(\'Note\',\''+dt+'\',\'\');">View Note</btn>';
					}
				},
			]
		});
	}


	$("#stock_form").on('submit', function(e){
		e.preventDefault();
		let id = $('#stock_id').val();
		let url = $(this).attr('action');
		let type = $("#type").val();
		let formData = $(this).serialize();
		$.ajax({
			type:"POST",
			url:url+'/'+id,
			data:formData,
			dataType:'json',
			beforeSend:function(){
				$('#stock_form_btn').prop('disabled', true);
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					swal("Success", response.message, "success");
					// show_stocks();
					$('#modal_stock_form').modal('hide');

					if(type == 'products'){
						show_products();
					}else if (type == 'materials') {
						show_materials();
					}

				}else{
					if(typeof response.details === 'undefined' || response.details  === null || response.details  == ''){
					}else{
						let details = response.details;

						let out_message = details.map(function(dt){
							return dt.invalid;
						}).join('\n');
						swal("Error", response.message+"\n\n"+out_message, "error");
					}
					// console.log(response);
				}
					validation('stock_form', response.error);
					$('#stock_form_btn').prop('disabled', false);
			},
			error: function(error){
				console.log(error);
			}
		});
			$('#stock_form_btn').prop('disabled', false);

	});

	function add_stocks(id, type, title, code){
		if(type == 'products'){
			$('#product_id').val(id);
			$('#material_id').val('');
		}else{
			$('#material_id').val(id);
		}

		$("#type").val(type);
		$("#title_item").text(title);
		$("#material_code").text(code);

		$("#stock_id").val('');
		$('#qty').val('');
		$('#status').val('');
		$('#note').val('');
		$("#modal_stock_form").modal('show');
	}


	$("#status").on('input', function(){
		if($(this).val() == 'stock_in'){
			$('.note').val('');
			$('.note').addClass('d-none');
			$('.note').attr('required');
		}else{
			$('.note').val('');
			$('.note').removeClass('d-none');
			$('.note').removeAttr('required');
		}
	});


	var tbl_stock_items;
	function view_stocks(id, type, title){
		$("#item_name_modal").text(title);

		$("#modal_stock_flow").modal('show');

		if(tbl_stock_items){
			tbl_stock_items.destroy();
		}
		tbl_stock_items = $('#tbl_stock_items').DataTable({
			destroy: true,
			pageLength: 10,
			responsive: true,
			ajax: "{{ route('stocks.find.items') }}/"+id+"/"+type,
			deferRender: true,
			columns: [
				{
					className: 'text-center',
					"data": 'user.name',
					"title": 'Authorized By',
				},{
					className: 'text-center',
					"data": 'qty_at_that_time',
					"title": 'Remaining Quantity',
					"render":function(data){
						return '<b>'+number_format.format(data)+'</b>';
					}
				},
				{
					className: 'text-center',
					"data": 'qty',
					"title": 'Added/Deducted QTY',
					"render": function(data, type, row, meta){
						if(row.status == 'stock_in'){
							return '<span class="text-success"><b>+'+number_format.format(data)+'</b></span>';
						}else{
							return '<span class="text-danger"><b>-'+number_format.format(data)+'</b></span>';

						}
					}
				},
				{
					className: 'text-right',
					"data": 'updated_qty',
					"title": 'Updated Qty',
					"render": function(data, type, row, meta){
						if(row.status == 'stock_in'){
							return '<span class="text-success h4"><b>'+number_format.format(data)+'</b></span>';
						}else{
							return '<span class="text-danger h4"><b>'+number_format.format(data)+'</b></span>';

						}
					}
				},
				{
					className: 'text-center',
					"width": '15',
					"data": 'status',
					"title": 'Status',
					"render":function(data){
						if(data == 'stock_in'){
							return '<span class="badge badge-success py-3 col-md-12">Stocked In</span>';
						}else{
							return '<span class="badge badge-danger py-3 col-md-12">Stocked Out</span>';
						}
					}
				},
				{
					className: 'text-center',
					"data": 'created_at',
					"title": 'Date Entry',
				},
				{
					className: 'text-center',
					"data": 'note',
					"title": 'Note',
					"render":function(data){
						let dt = (data === null || data === '')? 'Note is empty!' : data;
						return '<btn class="btn btn-info" onclick="swal(\'Note\',\''+dt+'\',\'\');">View Note</btn>';
					}
				},
			]
		});
	}


	function edit_stocks(stock_id){
		$.ajax({
			type:"GET",
			url:"{{ route('stocks.find') }}/"+stock_id,
			data:{},
			dataType:'json',
			beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					$('#stock_id').val(response.data.stock_id);
					$('#product_id').val(response.data.product_id);
					$('#material_id').val(response.data.material_id);
					$('#qty').val(response.data.qty);
					$('#status').val(response.data.status);
					$('#note').val(response.data.note);
					$('#modal_stock_form').modal('show');
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
		});
	}


	function delete_stocks(stock_id){
		swal({
			title: "Are you sure?",
			text: "Do you want to delete stocks?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes",
			closeOnConfirm: false
		},
		function(){
			$.ajax({
				type:"DELETE",
				url:"{{ route('stocks.delete') }}/"+stock_id,
				data:{},
				dataType:'json',
				beforeSend:function(){
			},
			success:function(response){
				// console.log(response);
				if (response.status == true) {
					show_stocks();
					swal("Success", response.message, "success");
				}else{
					console.log(response);
				}
			},
			error: function(error){
				console.log(error);
			}
			});
		});
	}


</script>