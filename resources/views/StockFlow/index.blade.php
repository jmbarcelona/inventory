@extends('Layout.admin_app')
@section('title', 'Stock Flow')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>{{ $type }} Stock Flow</h2></div>
		<div class="col-md-6 text-right"><button class="btn btn-success" onclick="show_scanner();">Scan Code</button></div>
		<!-- <div class="col-md-6 text-right"><button type="button" onclick="add_stocks();" class="btn btn-primary">Add Stocks</button></div> -->
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_stocks" style="width: 100%;"></table>
		</div>
	</div>
</div>


<form id="scanner_form" class="needs-validation" action="{{ route('stocks.find.items.code') }}" >
	<div class="modal fade" role="dialog" id="scanner_modal" data-backdrop="static">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <div class="modal-title text-capitalize">
	       		{{ $type }}
	        </div>
	        <button class="close" onclick="$('#scanner_modal').modal('hide');">&times;</button>
	      </div>
	      <div class="modal-body">
	      	<div class="text-center">
	      		<h2>Please Scan the item code</h2>
	      		<img src="{{ asset('img/scan.gif') }}" class="img-fluid" alt="">
	      	</div>
	        <input type="hidden" id="type_scanned" name="type_scanned" value="{{ strtolower($type) }}" required>
	        <input type="text" id="scanned_code" name="scanned_code" class="form-control" required style="opacity: 0;">
	      </div>
	    </div>
	  </div>
	</div>
</form>
@include('StockFlow.modal')
@endsection


@section('script')
@include('StockFlow.script')

<script type="text/javascript">
	show_stocks('{{ $type ?? "" }}');


	function show_scanner(){
		$("#scanner_modal").modal('show');
		$("#scanned_code").val('');
		setTimeout(function(){
			$("#scanned_code").focus();
		},1000);
	}

	$("#scanner_form").on('submit', function(event){
		event.preventDefault();
		$("#scanner_modal").modal('show');
		let type_scanned = $("#type_scanned").val();
		let scanned_code = $("#scanned_code").val();
		let url = $(this).attr('action')+'/'+scanned_code+'/'+type_scanned;
		// alert(url);
		$.ajax({
		    type:"GET",
		    url:url,
		    data:{},
		    dataType:'json',
		    beforeSend:function(){
		    },
		    success:function(response){
		      console.log(response);

		     if (response.status == true) {
		      // console.log(response);
		     	if (type_scanned == 'products') {
		     		add_stocks(response.data.id, type_scanned, response.data.product_name, response.data.product_code);
		     	}else{
		     		add_stocks(response.data.id, type_scanned, response.data.material_name, response.data.item_code);
		     	}
		     }else{
		        swal("Error", response.message, "error");
		     }
		    },
		    error: function(error){
  		        swal("Error", "Please contact system administrator!", "error");
		    }
		  });
	});
</script>
@endsection
