<form class="needs-validation" id="stock_form" action="{{ route('stocks.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_stock_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<b><h5 class="modal-title"> <span id="title_item">Add Stocks</span> <small><span id="material_code"></span></small></h5></b>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12 d-none">
							<input type="hidden" name="stock_id" id="stock_id" />
							<input type="hidden" name="product_id" id="product_id" class="form-control "   />
							<input type="hidden" name="material_id" id="material_id" class="form-control " />
							<input type="hidden" name="type" id="type" class="form-control " />
						</div>
						<div class="col-md-12">
							<label>Qty</label>
							<input type="number" name="qty" id="qty" class="form-control" placeholder="Please enter the item quantity" required/>
						</div>
						<div class="col-md-12">
							<label>Status</label>
							<select name="status" id="status" class="form-control " required>
								<option value="" selected>Select Status</option>
								<option value="stock_in">Stock In</option>
								<option value="stock_out">Stock Out</option>
							</select>
						</div>
						<div class="col-md-12 note d-none">
							<label>Note</label>
							<textarea type="textarea" name="note" id="note" class="form-control " required></textarea>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="stock_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>

 <div class="modal fade" role="dialog" id="modal_stock_flow">
  <div class="modal-dialog modal-lg" style="min-width: 85% !important;">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title">
        	<h5>Stock Flow: <b><span id="item_name_modal"></span></b></h5>
        </div>
        <button class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="table-responsive">
			<table class="table table-bordered table-striped" id="tbl_stock_items" style="width: 100%;"></table>
        </div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>