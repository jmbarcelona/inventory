@extends('Layout.admin_app')
@section('title', 'Products')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Products Management</h2></div>
		<div class="col-md-6 text-right">
			<a href="{{ route('products.add') }}" class="btn btn-primary">Add Products</a>
			<button type="button" onclick="$('.btn_print').click();" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
			<button type="button" onclick="$('.excelButton').click();" class="btn btn-success"><i class="fa fa-file-excel"></i> Export Excel</button>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_products" style="width: 100%;"></table>
		</div>
	</div>
</div>
@include('StockFlow.modal')
@endsection

@section('script')
@include('StockFlow.script')
<script type="text/javascript">
var tbl_products;
show_products();
function show_products(){
	if(tbl_products){
		tbl_products.destroy();
	}
	tbl_products = $('#tbl_products').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {extend : 'print', 
            className: 'btn_print d-none',
             customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="{{ asset('img/logo.png') }}"/><div class="h3">Ace Plastic</div></center>'
                        );
                },
                exportOptions: {
                        stripHtml : false,
                        columns: [0, 2, 3, 4]
                }
		},
            { 
            	extend: 'excel', 
           		className: 'excelButton d-none',
           		exportOptions: {
                        columns: [0, 2, 3, 4]
                } 
            }
        ],
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('products.list') }}",
		deferRender: true,
		columns: [
			{
				className: 'd-none',
				"data": 'product_code',
				"title": 'Barcode',
			},{
				width: 90,
				className: 'text-center hide-print',
				"data": 'product_code',
				"title": 'Barcode',
				"render":function(data, type, row, meta){
					return '<svg class="qrcodes d-none" id="qrcode_'+data+'" data-value="'+data+'"></svg> <img id="img_qrcode_'+data+'">';

				}
			},
			{
				className: '',
				"data": 'product_name',
				"title": 'Product name',
			},
			{
				className: 'text-center',
				"data": 'qty',
				"title": 'QTY',
				"render":function(data, type, row, meta){
					if(data == '' || data == 0 || data == null){
						return '0';
					}else{
						return data;
					}
				}
			},
			{
				className: 'text-right',
				"data": 'price',
				"title": 'Price',
				"render": function(data){
					return money.format(data);
				}
			},
			{
				className: 'width-option-1 text-center hide-print',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<a href="{{ route('products.edit') }}/'+row.id+'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i></a> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_products('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					newdata += ' <button class="btn btn-primary btn-sm font-base mt-1"  onclick="add_stocks('+row.id+', \'products\', \''+row.product_name+'\', \'#'+row.product_code+'\')" type="button"><i class="fas fa-box-tissue"></i></button>';
					return newdata;
				}
			}
		]
	});

	setTimeout(function(){
		qrcodeRender();
	},1500);
}




function delete_products(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete products?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('products.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_products();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}
</script>
@endsection