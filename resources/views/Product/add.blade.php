@extends('Layout.admin_app')
@section('title', $title)
@section('css')
<style type="text/css">
	.img-container {
	  position: relative;
	  width: 50%;
	}

	.img-image {
	  opacity: 1;
	  display: block;
	  width: 100%;
	  height: auto;
	  transition: .5s ease;
	  backface-visibility: hidden;
	}

	.middle {
	  transition: .5s ease;
	  opacity: 0;
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  -ms-transform: translate(-50%, -50%);
	  text-align: center;
	}

	.img-container:hover .img-image {
	  opacity: 0.3;
	}

	.img-container:hover .middle {
	  opacity: 1;
	}

	.img-text {
/*	  background-color: #04AA6D;
	  color: white;
	  font-size: 16px;
	  padding: 16px 32px;*/
	}
</style>
@endsection
@section('content')
<div class="container-fluid pt-3">
	<div class="row">
		<div class="col-md-12"><h1>{{ $title }}</h2></div>
		<div class="col-md-12">
			<form class="needs-validation" id="product_form" action="{{ route('products.save') }}/{{ $products->id ?? '' }}" novalidate>
				<div class="row">
					<input type="hidden" name="id" id="id" value="{{ $products->id ?? '' }}"/>
					<div class="col-md-8">
						<div class="card">
							<div class="card-header">Product Details</div>
							<div class="card-body">
								<div class="row">
									<div class="col-md-6">
										<label>Product code</label>
										<input type="text" name="product_code" id="product_code" class="form-control form-control" required value="{{ $products->product_code ?? '' }}"/>
									</div>
									<div class="col-md-6">
										<label>Stock Alert <small class="text-info"><b> The System will alert you if the stocks is less than/qual to this value.</b></small></label>
										<input type="number" name="alert_qty" id="alert_qty" class="form-control form-control" required value="{{ $products->alert_qty ?? '' }}"/>
									</div>
									<div class="col-md-12">
										<label>Product name</label>
										<input type="text" name="product_name" id="product_name" class="form-control form-control" required value="{{ $products->product_name ?? '' }}"/>
									</div>
									<div class="col-md-12">
										<label>Product Description</label>
										<textarea name="description" id="description" class="form-control form-control">{{ $products->description ?? '' }}</textarea>
									</div>
								</div>
							</div>
						</div>

						<div class="card">
							<div class="card-header">Raw Materials</div>
							<div class="card-body">
								<table class="table table-bordered">
									<thead>
										<tr>
											<th>Material Name</th>
											<th>Kg</th>
											<th class="text-center">Price/KG</th>
											<th class="text-center">Options</th>
										</tr>
									</thead>
									<tbody id="raw_materials_item">
									</tbody>
								</table>

								<div class="text-right mt-5">
									<button class="btn btn-primary" type="button" onclick="add_item();"><i class="fa fa-plus"></i> Add Items</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="card">
							<div class="card-header">Product Image</div>
							<div class="card-body">
								<div class="text-center">
									<input type="file" name="image" id="image" onchange="previewImg(this, 'product_image_preview');" class="d-none form-control">
									<center>
										<div class="img-container">
											<img src=" {{ (!empty($products->image))? asset('products/'.$products->image) :  asset('img/default.jpg') }}" alt="product-image" id="product_image_preview" class="img-fluid img-image">
										  <div class="middle">
										    <button class="btn btn-primary" type="button" onclick="$('#image').click();">Upload Image</button>
										  </div>
										</div>
									</center>
								</div>	
							</div>
						</div>
						<div class="card">
							<div class="card-header">Pricing</div>
							<div class="card-body">
								<div class="col-md-12 mb-3">
									<label>Suggested Price <small class="text-info">This price is based on the material's price.</small></label>
									<div class="input-group mb-3">
										 <div class="input-group-prepend">
										    <span class="input-group-text" id="basic-addon1">₱</span>
										  </div>
  										<input type="text" name="suggested_price" id="suggested_price" class="form-control form-control form-control-lg bg-white" placeholder="0" aria-label="Recipient's username" aria-describedby="button-addon2" readonly />
									  	<button class="btn btn-outline-success rounded-0" type="button" onclick="$('#price').val($('#suggested_price').val());" id="button-addon2">Use Price</button>
									</div>
								</div>
								<div class="col-md-12 mb-3">
									<label>Price</label>
									<div class="input-group">
										<div class="input-group-prepend">
										   <span class="input-group-text" id="basic-addon1">₱</span>
										 </div>
										<input type="text" name="price" id="price" class="form-control form-control form-control-lg" placeholder="0" required value="{{ $products->price ?? '' }}"/>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<a href="{{ route('products.index') }}" class="btn btn-dark btn-block" type="button">Cancel</a>
							</div>
							<div class="col-md-6">
								<button class="btn btn-success btn-block" id="product_form_btn" type="submit">Save</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>


@endsection
@section('script')
<script type="text/javascript">
var default_value = '{!! $products->product_details ?? '' !!}';

renderData(default_value);


function renderData(default_value){
	if (default_value == '') {

	}else{
		let data = JSON.parse(default_value);
		data.map(function(dt){
			add_item(dt.material_id, dt.qty, dt.price);
		});
	}
}


var counter = 0;
function add_item(material_id = '', qty = '', price = 0){
	let output = '';
	output += '<tr id="item_'+counter+'">';
	output += '<td width="300">';
	output += '<select name="details['+counter+'][material_id]" id="material_id_'+counter+'" value="'+material_id+'" oninput="get_selected_data('+counter+')" class="form-control materials"></select>';
	output += '</td>';
	output += '<td><input type="number" name="details['+counter+'][qty]" id="qty_'+counter+'" value="'+qty+'" placeholder="0" oninput="get_total_price_from_materials();" class="form-control"></td>';
	output += '<td class="text-center item_price" data-id="'+counter+'" id="price_'+counter+'">'+0+'</td>';
	output += '<td class="text-center"><button class="btn btn-danger" onclick="remove_item('+counter+');" type="button"><i class="fa fa-times"></i></button></td>';
	output += '</tr>';
	$("#raw_materials_item").append(output);
	render_option('material_id_'+counter, material_id);
	get_selected_data(counter);
	get_total_price_from_materials();
	counter++;

}


function render_option(element, selected = ''){
	let data = JSON.parse('@json($materials)');
	
	 let output ='<option value="" selected>Select Material</option>';
	 output += data.map(function(dt){
		let out = '';
		out += '<option value="'+dt.id+'" data-price="'+dt.price_per_kilo+'">'+dt.material_name+'</option>';
		return out;
	}).join('');

	$("#"+element).html(output);
	$("#"+element).val(selected);
}


function get_selected_data(counter){
	let price = $("#material_id_"+counter).find(':selected').data('price');
	$("#price_"+counter).text(price);

	get_total_price_from_materials();
}

function get_total_price_from_materials(){
	var total = 0;
	$(".item_price").each(function(e){
		let id = $(this).data('id');
		let num = Number($(this).text());
		let qty = Number($("#qty_"+id).val());
		total += (qty * num);
	});

	$("#suggested_price").val(total);
}

function remove_item(counter){
  var url = '';
  swal({
        title: "Are you sure?",
        text: "Do you want to remove this item?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: true
      },
        function(){
      		$("#item_"+counter).remove();
			get_total_price_from_materials();
      });
  }


$('#product_form').on('submit', function(e){
  e.preventDefault();
  let form = $("#product_form")[0];
  var data = new FormData(form);
  let url = $(this).attr('action');
  $.ajax({
      type:"POST",
      url:url,
      data:data,
      enctype: 'multipart/form-data',
      processData: false,  // Important!
      contentType: false,
      dataType:'json',
      beforeSend:function(){
		$('#product_form_btn').prop('disabled', true);
      },
      success:function(response){
         console.log(response);
  		if (response.status == true) {
			swal("Success", response.message, "success");
			setTimeout(function(){
				window.location = "{{ route('products.index') }}";
			}, 1500);
		}else{
			console.log(response);
		}
		validation('product_form', response.error);
		$('#product_form_btn').prop('disabled', false);
      },
      error: function(error){
        console.log(error);
      }
    });
});
</script>
@endsection