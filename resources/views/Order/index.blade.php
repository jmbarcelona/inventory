@extends('Layout.admin_app')
@section('title', 'Orders')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Orders Management</h2></div>
		<div class="col-md-6 text-right">
			<a href="{{ route('orders.add') }}" class="btn btn-primary">Add Orders</a>
			<button type="button" onclick="$('.btn_print').click();" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
			<button type="button" onclick="$('.excelButton').click();" class="btn btn-success"><i class="fa fa-file-excel"></i> Export Excel</button>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_orders" style="width: 100%;"></table>
		</div>
	</div>
</div>

<form class="needs-validation" id="payment_order_form" action="{{ route('orders.payment') }}" novalidate="">
<div class="modal fade" role="dialog" id="payment_modal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<div class="modal-title">
					Add Payment
				</div>
				<button class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12">
						<div class="card bg-success text-white">
							<div class="card-body">
								<label id="order_no">Order#123123</label><br>
								<label>Total Bill</label>
								<h1 class="m-1 bg-success" id="bill_text">P 0.00</h1>
							</div>
						</div>
						<input type="hidden" name="bill" id="bill" class="form-control form-control-lg" required/>
						<input type="hidden" name="order_id" id="order_id"/>
					</div>
					<div class="col-md-12">
						<div class="card bg-primary text-white">
							<div class="card-body">
								<label>Change </label>
								<h1 class="m-1" id="change_text">P 0.00</h1>
							</div>
						</div>
						<input type="hidden" name="change" id="change" class="form-control form-control-lg" value="0" required disabled="" />
					</div>
					<div class="col-md-12 mb-3">
						<label>Amount Tendered </label>
						<input type="number" name="amount_tendered" id="amount_tendered" oninput="enter_payment(this.value);" class="form-control form-control-lg" placeholder="0" required/>
					</div>
					<div class="col-md-12 form-group">
						<button class="btn btn-primary btn-block" type="submit">Process Payment</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</form>

@endsection


@section('script')
<!-- Javascript Function-->
<script>
function enter_payment(value){
	let bill = $("#bill").val();
	let total_amount = Number(value) - Number(bill);
	
	if (isNaN(total_amount)) {
		$("#change").val(0);
		$("#change_text").text('₱0.00');
	}else{
		$("#change_text").text(money.format(total_amount));
		$("#change").val(total_amount);
	}
}

$("#payment_order_form").on('submit', function(e){
    e.preventDefault();
    let url = $(this).attr('action');
    let data = $(this).serialize();
    $.ajax({
        type:"POST",
        url:url,
        data:data,
        dataType:'json',
        beforeSend:function(){
        },
        success:function(response){
         	console.log(response);
             if (response.status == true) {
             	swal("Success", response.message, 'success');
				$("#payment_modal").modal('hide');
				show_orders();
             }

			validation('payment_order_form', response.error);

        },
        error: function(error){
          console.log(error);
        }
      });
});


function add_payment(order_id, order_tracking_no, bill){
	$("#order_id").val(order_id);
	$("#order_tracking_no").text(order_tracking_no);
	$("#bill").val(bill);
	$("#bill_text").text(money.format(bill));
	$("#payment_modal").modal('show');
}

var tbl_orders;
show_orders();
function show_orders(){
	if(tbl_orders){
		tbl_orders.destroy();
	}
	tbl_orders = $('#tbl_orders').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {extend : 'print', 
            className: 'btn_print d-none',
             customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<center><img src="{{ asset('img/logo.png') }}"/><div class="h3">Ace Plastic</div></center>'
                        );
                },
                exportOptions: {
                        stripHtml : true,
                        columns: [0, 1, 2, 3]
                }
		},
            { 
            	extend: 'excel', 
           		className: 'excelButton d-none',
           		exportOptions: {
                        columns: [0, 1, 2, 3]
                } 
            }
        ],
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('orders.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'order_tracking_no',
				"title": 'Order No',
			},
			{
				className: '',
				"data": 'customer_id',
				"title": 'Customer',
				"render":function(data, type, row, meta){
					return row.customer.customer_name+' '+row.customer.contact_person;
				}
			},
			{
				className: '',
				"data": 'delivery_date',
				"title": 'Delivery date',
			},
			{
				className: 'text-center',
				"data": 'status',
				"title": 'Status',
				"render":function(data){
					if (data == 1) {
						return '<span class="badge badge-success">Completed</span>';
					}else{
						return '<span class="badge badge-warning">Pending</span>';
					}
				}
			},
			{
				className: 'text-center',
				"data": 'total_amount',
				"title": 'Total Amount',
				"render":function(data){
					return money.format(data);
				}
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<a href="{{ route('orders.edit') }}/'+row.id+'" class="btn btn-success btn-sm font-base mt-1" ><i class="fa fa-edit"></i></a> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_orders('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					if (row.status == 1) {
						newdata += ' <button class="btn btn-info btn-sm font-base mt-1" type="button" disabled><i class="fa fa-money-bill-alt"></i></button>';

					}else{
						newdata += ' <button class="btn btn-info btn-sm font-base mt-1" title="Mark as paid" onclick="add_payment('+row.id+', \''+row.order_tracking_no+'\', \''+row.total_amount+'\');" type="button"><i class="fa fa-money-bill-alt"></i></button>';						
					}
					return newdata;
				}
			},
		]
	});
}


function delete_orders(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete orders?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('orders.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_orders();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
