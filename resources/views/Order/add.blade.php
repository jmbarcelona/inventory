@extends('Layout.admin_app')
@section('title', $title)
@section('css')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
<div class="container-fluid pt-3">
	<div class="row">
		<div class="col-md-12"><h1>{{ $title }} <small class="float-right"><b>{{ (!empty( $orders->order_tracking_no))? $orders->order_tracking_no : '' }}</b></small></h2></div>
		<div class="col-md-12">
			<form class="needs-validation" id="item_form" action="{{ route('orders.get.items') }}" novalidate>
				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<input type="text" class="form-control form-control-lg" id="item_code" name="item_code" placeholder="Type or Scan Item code">
						</div>
					</div>
				</div>
			</form>

			<form class="needs-validation" id="order_form" action="{{ route('orders.save') }}/{{ $orders->id ?? '' }}" novalidate>
				<div class="row">
					<input type="hidden" name="id" id="id" value="{{ $orders->id ?? '' }}"/>
					<div class="col-md-3">
						<div class="card">
							<div class="card-body">
								<div class="row">
									<div class="col-md-12 form-group">
										<label>Customer Name</label>
										<select name="customer" id="customer" class="form-control selection"  data-value="{{ $orders->customer_id ?? '' }}"  required {{ (!empty($orders->status) && $orders->status == 1)? 'disabled' : '' }}>
											<option value="" selected>Please Select Customer</option>
											@foreach(getCustomerOption() as $customer)
											<option value="{{ $customer->id }}">{{ $customer->customer_name }} - {{ $customer->contact_person }}</option>
											@endforeach
										</select>
									</div>
									<div class="col-md-12 form-group">
										<label>Order/Delivery Date</label>
										<input type="date" name="delivery_date" id="delivery_date" class="form-control " required value="{{ $orders->delivery_date ?? '' }}" {{ (!empty($orders->status) && $orders->status == 1)? 'disabled' : '' }}>
									</div>
								</div>
								<h4 class="text-right mt-3">Total Bill</h4>
								<h3 class="text-right" id="total_bill_text">₱{{ (!empty($orders->total_amount))? number_format($orders->total_amount, 2) : '0.00' }}</h3>
								<div class="form-group text-right">
									<input type="hidden" name="total_bill" id="total_bill" class="form-control" value="{{ $orders->total_amount ?? '' }}">
								</div>
									<button class="btn btn-primary btn-block {{ (!empty($orders->status) && $orders->status == 1)? 'd-none' : '' }}">Save Order</button>
							</div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="card">
							<div class="card-header">Items</div>
							<div class="card-body">
								<table class="table table-bordered" style="width: 100%;">
									<thead>
										<tr>
											<th class="text-center" width="25">Item Code</th>
											<th class="text-center" width="220">Item Name</th>
											<th class="text-center" width="25">Price</th>
											<th width="25" class="text-center">Qty</th>
											<th width="15" class="text-center">Actions</th>
										</tr>
									</thead>
									<tbody id="order_items">
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>



@endsection


@section('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
var products = [];
var default_value = '{!! $orders->items ?? '' !!}';

renderData(default_value);

function renderData(default_value){
	if (default_value == '') {

	}else{
		let data = JSON.parse(default_value);
		data.map(function(dt){
			append_items(dt.product_id, dt.products.product_code, dt.products.product_name, dt.price, dt.stock, dt.qty);
		});
	}
}


$("#item_form").on('submit', function(e){
	e.preventDefault();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url,
		data:formData,
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			console.log(response);
			if (response.status == true) {
				append_items(response.data.id, response.data.product_code, response.data.product_name, response.data.price, response.data.qty, 1);	
				$("#item_code").val('');
				$("#item_code").focus();
			}
			validation('item_form', response.error);
		},
		error: function(error){
			console.log(error);
		}
	});
});


function append_items(id, item_code, name, price, stock, qty){
	let new_qty = qty;
	let output = '';

	if (id in products) {
		new_qty = (Number(qty) + Number(products[id]));		
	}

	if (new_qty > stock) {
		swal("Error", "The product stock(s) is only "+stock+" left.\n Replenish this product to be able to continue.", "error");
	}else{
		if (id in products) {
			$("#item_qty_"+id).val(new_qty);
		}else{
			output +='<tr class="cart_items" id="cart_item_'+id+'" data-id="'+id+'" data-price="'+price+'" data-stock="'+stock+'">';
			output +='<td  class="text-center">'+item_code+'<input type="hidden" name="items['+id+'][price]" value="'+price+'" class="form-control"></td>';
			output +='<td  class="text-center">'+name+'</td>';
			output +='<td  class="text-center">'+money.format(price)+'</td>';
			output +='<td  class="text-center"><input type="hidden" name="items['+id+'][id]" value="'+id+'" class="form-control"><input type="number" name="items['+id+'][qty]" id="item_qty_'+id+'" oninput="return edit_qty('+id+')" value="'+new_qty+'" class="form-control text-center"></td>';

			@if(!empty($orders->status) && $orders->status == 1)
				output +='<td class="text-center"><button class="btn btn-danger"  type="button" disabled><i class="fa fa-times"></i></button></td>';
			@else
				output +='<td class="text-center"><button class="btn btn-danger"  type="button" onclick="remove_cart('+id+');"><i class="fa fa-times"></i></button></td>';
			@endif
			output +='</tr>';

			$("#order_items").append(output);
		}
		products[id] = new_qty;
		get_all_items();
	}
}


function edit_qty(id){
	let qty = $("#item_qty_"+id).val();
	let stock = $("#cart_item_"+id).data('stock');

	if (Number(qty) > Number(stock)) {
		 $("#item_qty_"+id).val(stock);
	}else if(Number(qty) < 1){
		 $("#item_qty_"+id).val(1);
	}else{
		get_all_items();
	}
}

function get_all_items(){
	products = [];
	let total = 0;
	$(".cart_items").each(function(e){
		let id = $(this).data('id');
		let price = $(this).data('price');
		let qty = $("#item_qty_"+id).val();
		products[id] = qty;
		total += (Number(qty) * Number(price));
	});

	$("#total_bill").val(total);
	$("#total_bill_text").text(money.format(total));
}



function remove_cart(id){
  swal({
        title: "Are you sure?",
        text: "Do you want to remove this item?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes",
        closeOnConfirm: true
      },
        function(){
         $("#cart_item_"+id).remove();
         get_all_items();
      });
  }

$("#order_form").on('submit', function(e){
	e.preventDefault();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#order_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				setTimeout(function(){
					window.location = "{{ route('orders.index') }}";
				}, 1500);
			}else{
				console.log(response);
			}
				validation('order_form', response.error);
				$('#order_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#order_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

$(".selection").each(function(e){
	let selected = $(this).data('value');
	$(this).val(selected);
});
</script>
@endsection