@extends('Layout.admin_app')
@section('title', 'Activity Logs')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Activity Logs</h2></div>
		<div class="col-md-6 text-right">

			<button type="button" onclick="$('.btn_print').click();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button>
			<button type="button" onclick="$('.excelButton').click();" class="btn btn-success"><i class="fa fa-file-excel"></i> Export Excel</button>
		</div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_logs" style="width: 100%;"></table>
		</div>
	</div>
</div>
@endsection


@section('script')
<script type="text/javascript">
var tbl_logs;
show_logs();
function show_logs(){
	if(tbl_logs){
		tbl_logs.destroy();
	}
	tbl_samples = $('#tbl_logs').DataTable({
		dom: 'Bfrtip',
        buttons: [
            {extend : 'print', className: 'btn_print d-none'},
            { extend: 'excel', className: 'excelButton d-none' }
        ],
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('logs.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'user.name',
				"title": 'Authorized By',
			},
			{
				className: '',
				"data": 'title',
				"title": 'Title',
			},
			{
				className: '',
				"data": 'message',
				"title": 'Message',
			},
			{
				className: '',
				"data": 'created_at',
				"title": 'Date',
			}
		]
	});
}

</script>
@endsection
