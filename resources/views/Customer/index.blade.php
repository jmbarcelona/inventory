@extends('Layout.admin_app')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-6 text-capitalize"><h1>Customers Management</h2></div>
		<div class="col-md-6 text-right"><button type="button" onclick="add_customers();" class="btn btn-primary">Add Customers</button></div>
		<div class="col-md-12">
			<table class="table table-bordered table-striped" id="tbl_customers" style="width: 100%;"></table>
		</div>
	</div>
</div>

<!-- Modal Form -->
<form class="needs-validation" id="customer_form" action="{{ route('customers.save') }}" novalidate>
	<div class="modal fade" tabindex="-1" role="dialog" id="modal_customer_form" data-backdrop="static">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="modal_customer_form_title">Add Customers</h5>
					<button class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<input type="hidden" name="id" id="id" />
						<div class="col-md-12">
							<label>Customer name</label>
							<input type="text" name="customer_name" id="customer_name" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Contact person</label>
							<input type="text" name="contact_person" id="contact_person" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Address</label>
							<input type="text" name="address" id="address" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Contact No.</label>
							<input type="text" name="contact_number" id="contact_number" class="form-control " required />
						</div>
						<div class="col-md-12">
							<label>Email</label>
							<input type="email" name="email" id="email" class="form-control " required />
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="submit" class="btn btn-success" id="customer_form_btn" >Save</button>
				</div>
			</div>
		</div>
	</div>
</form>
@endsection


@section('script')
<!-- Javascript Function-->
<script>
var tbl_customers;
show_customers();
function show_customers(){
	if(tbl_customers){
		tbl_customers.destroy();
	}
	tbl_customers = $('#tbl_customers').DataTable({
		destroy: true,
		pageLength: 10,
		responsive: true,
		ajax: "{{ route('customers.list') }}",
		deferRender: true,
		columns: [
			{
				className: '',
				"data": 'customer_name',
				"title": 'Customer name',
			},
			{
				className: '',
				"data": 'contact_person',
				"title": 'Contact person',
			},
			{
				className: '',
				"data": 'address',
				"title": 'Address',
			},
			{
				className: '',
				"data": 'contact_number',
				"title": 'Contact No.',
			},
			{
				className: '',
				"data": 'email',
				"title": 'Email',
			},
			{
				className: 'width-option-1 text-center',
				width: '15%',
				"data": 'id',
				"orderable": false,
				"title": 'Options',
				"render": function(data, type, row, meta){
					newdata = '';
					newdata += '<button class="btn btn-success btn-sm font-base mt-1"  onclick="edit_customers('+row.id+')" type="button"><i class="fa fa-edit"></i></button> ';
					newdata += ' <button class="btn btn-danger btn-sm font-base mt-1" onclick="delete_customers('+row.id+');" type="button"><i class="fa fa-trash"></i></button>';
					return newdata;
				}
			}
		]
	});
}


$("#customer_form").on('submit', function(e){
	e.preventDefault();
	let id = $('#id').val();
	let url = $(this).attr('action');
	let formData = $(this).serialize();
	$.ajax({
		type:"POST",
		url:url+'/'+id,
		data:formData,
		dataType:'json',
		beforeSend:function(){
			$('#customer_form_btn').prop('disabled', true);
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				swal("Success", response.message, "success");
				show_customers();
				$('#modal_customer_form').modal('hide');
			}else{
				console.log(response);
			}
				validation('customer_form', response.error);
				$('#customer_form_btn').prop('disabled', false);
		},
		error: function(error){
			$('#customer_form_btn').prop('disabled', false);
			console.log(error);
		}
	});
});

function add_customers(){
	$("#id").val('');
	$('#customer_name').val('');
	$('#contact_person').val('');
	$('#address').val('');
	$('#contact_address').val('');
	$('#email').val('');
	$("#modal_customer_form").modal('show');
}


function edit_customers(id){
	$.ajax({
		type:"GET",
		url:"{{ route('customers.find') }}/"+id,
		data:{},
		dataType:'json',
		beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				$('#id').val(response.data.id);
				$('#customer_name').val(response.data.customer_name);
				$('#contact_person').val(response.data.contact_person);
				$('#address').val(response.data.address);
				$('#contact_address').val(response.data.contact_address);
				$('#email').val(response.data.email);
				$('#modal_customer_form').modal('show');
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
}


function delete_customers(id){
	swal({
		title: "Are you sure?",
		text: "Do you want to delete customers?",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes",
		closeOnConfirm: false
	},
	function(){
		$.ajax({
			type:"DELETE",
			url:"{{ route('customers.delete') }}/"+id,
			data:{},
			dataType:'json',
			beforeSend:function(){
		},
		success:function(response){
			// console.log(response);
			if (response.status == true) {
				show_customers();
				swal("Success", response.message, "success");
			}else{
				console.log(response);
			}
		},
		error: function(error){
			console.log(error);
		}
		});
	});
}

</script>
@endsection
