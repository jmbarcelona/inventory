@extends('Layout.app')
@section('title', 'San Isidro Official Website')
@section('content')
	<div class="row mt-3">
		<div class="col-md-6 col-lg-6 col-12">
			<h1 class="third-bg text-light p-2 text-center">Activities</h1>
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
			    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
			    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  </ol>
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img class="d-block w-100" src="{{ asset('img/sample.jpg') }}" alt="First slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="{{ asset('img/sample.jpg') }}" alt="third slide">
			    </div>
			    <div class="carousel-item">
			      <img class="d-block w-100" src="{{ asset('img/sample.jpg') }}" alt="Third slide">
			    </div>
			  </div>
			  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
			    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
			    <span class="sr-only">Previous</span>
			  </a>
			  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    <span class="carousel-control-next-icon" aria-hidden="true"></span>
			    <span class="sr-only">Next</span>
			  </a>
			</div>
		</div>
		<div class="col-md-6 col-lg-6 col-12">
			<h1 class="third-bg text-light p-2 text-center">News and Updates</h1>
			<div class="card card-primary">
				<div class="card-header"><h4><strong>President Visit</strong></h4></div>
				<div class="card-body">
					<p>President Ferdinand 'Bong-Bong' Marcos is visitng our province in the 3rd day of Novemeber.</p>
				</div>
				<div class="card-footer"></div>
			</div>
			<div class="card card-success">
				<div class="card-header"><h4><strong>Job Hunt</strong></h4></div>
				<div class="card-body">
					<p class="m-1">Must be at least 23 and above</p>
					<p class="m-1">High School/College graduate</p>
					<p class="m-1">With or without skills</p>
					<br>
					<p class="m-1">Please Contact: 0943-231-1112 or send an email to sample69@gmail.com</p>
				</div>
				<div class="card-footer"></div>
			</div>
		</div>
	</div>
@endsection

