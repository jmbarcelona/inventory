@extends('Layout.admin_app')
@section('title', 'Decision Support')
@section('content')
<div class="container-fluid pt-3">
	<div class="row mb-3">
		<div class="col-md-12 text-capitalize"><h1>Decision Support</h2>
			<div class="alert alert-danger alert-dismissible" role="alert">
			  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			  <strong><i class="fa fa-exclamation-circle"></i></strong> Please Take Action.
			</div>
		</div>
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header bg-danger text-white">
					<h1 class="card-title">List of Material that need to re-stock</h1>
				</div>
				<div class="card-body">
					<table class="table table-bordered" id="table_materials" style="width: 100%;">
						<thead>
							<tr>
								<th>Name</th>
								<th>Qty</th>
							</tr>
						</thead>
						<tbody>
							@foreach(getAlertMaterials() as $material)
							<tr>
								<td>{{ $material->material_name }}</td>
								<td>{{ $material->qty }} KG</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="card-footer"></div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="card">
				<div class="card-header bg-danger text-white">
					<h1 class="card-title">List of Products that need to re-stock</h1>
				</div>
				<div class="card-body">
					<table class="table table-bordered" id="table_products" style="width: 100%;">
						<thead>
							<tr>
								<th>Name</th>
								<th>Qty</th>
							</tr>
						</thead>
						<tbody>
							@foreach(getAlertProducts() as $product)
							<tr>
								<td>{{ $product->product_name }}</td>
								<td>{{ $product->qty }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="card-footer"></div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('script')

@endsection
